        <!-- Main Sidebar Container -->

        {{-- #000035 --}}
        {{-- #084E11 --}}          
        <aside class="main-sidebar sidebar-dark-light elevation-4 position-fixed" style="color: white; background-color:#000035;">
            <!-- Brand Logo -->
            <a href="{{asset('admin/index3.html')}}" class="brand-link">
            <img src="{{asset('admin/fhi.png')}}" alt="#" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="fw-bold" >SIM-HOCKGRES</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{asset('admin/orang.png')}}" class="img-circle elevation-2" alt="User Image">
                    </div> 
                    <div class="info">
                        <a href="#" class="d-block">{{ Auth::user()->name}} ({{ Auth::user()->role}})</a>
                    </div>
                </div>
        
                <!-- SidebarSearch Form -->
                <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                    <input style="background-color: #000035;color: white" class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                    <button class="btn btn-sidebar " style="background-color: #000035;color: white">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                    </div>
                </div>
                </div>
    
            <!-- Sidebar Menu -->
            
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="/{{ Auth::user()->role }}/dashboard" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Dashboard
                            </p>
                        </a>
                    </li>
                    
                    @if (Auth::user()->role == 'manajer')
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user-cog"></i>
                            <p>
                                Setting Akun
                            <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="/manajer/akunatlet/dataatlet" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Akun Atlet</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/manajer/akunpelatih/datapelatih" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Akun Pelatih & Staff</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/kategori/datakategori" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Kategori</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'manajer' | Auth::user()->role == 'keuangan')
                    <li class="nav-item">
                        <a href="/{{Auth::user()->role}}/data-keuangan" class="nav-link">
                            <i class="nav-icon fas fa-chart-line"></i>
                            <p>Data Keuangan</p>
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'keuangan')
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon far fa-envelope-open"></i>
                            <p>
                                Setting Gaji
                            <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="/keuangan/setting_gaji" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Setting Gaji</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/keuangan/generategaji" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Generate Gaji</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/keuangan/pencairan" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pencairan Gaji</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'keuangan')
                    <li class="nav-item">
                        <a href="/keuangan/pengajuan-sarpras" class="nav-link">
                            <i class="nav-icon fas fa-handshake"></i>
                            <p>Approve Pengajuan</p>
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'pelatih')
                    <li class="nav-item">
                        <a href="/pelatih/data-atlet" class="nav-link">
                            <i class="nav-icon fas fa-database"></i>
                            <p>
                                Data Atlet
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'manajer'|| Auth::user()->role == 'pelatih'|| Auth::user()->role == 'atlet' )
                    <li class="nav-item">
                        <a href="/{{Auth::user()->role}}/jadwal/jadkom/datajadkom" class="nav-link">
                            <i class="nav-icon fas fa-trophy"></i>
                            <p>
                                Jadwal Kompetisi
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'pelatih'|| Auth::user()->role == 'atlet' )
                    <li class="nav-item">
                        <a href="/{{Auth::user()->role}}/jadwal/jadlat/datajadlat" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>
                                Jadwal Latihan
                            </p>
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'pelatih')
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="fas fa-address-book nav-icon"></i>
                            <p>
                                Absensi
                            <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="/{{Auth::user()->role}}/generateabsensi" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Generate Absensi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/{{Auth::user()->role}}/absensi" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Isi Absensi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/{{Auth::user()->role}}/dataabsensi/pilih_jadlat" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Absensi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'pelatih')
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="fas fa-trophy nav-icon"></i>
                            <p>
                                Seleksi Atlet
                            <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="/pelatih/datakriteria" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Kriteria</p>
                                </a>
                            </li><li class="nav-item">
                                <a href="/pelatih/data-sub_kriteria" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Sub-Kriteria</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/pelatih/datapenilaian/pilih-jadkom" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penilaian Atlet</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/pelatih/pilih-jadkom" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Hasil Seleksi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'atlet')
                    <li class="nav-item">
                        <a href="/{{Auth::user()->role}}/dataabsensi" class="nav-link">
                            <i class="fas fa-address-book nav-icon"></i>
                            <p>Absensi Latihan</p>
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'atlet')
                    <li class="nav-item">
                        <a href="/atlet/gaji" class="nav-link">
                            <i class="fas fa-dollar-sign nav-icon"></i>
                            <p>Gaji </p>
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'atlet')
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user-cog"></i>
                            <p>
                                Seleksi
                            <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="/atlet/pilih-jadkom/peniliaian" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Penilaian</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/atlet/pilih-jadkom" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Hasil Seleksi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'sarpras')
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-hammer"></i>
                            <p>
                                Sarana Prasarana
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview"> 
                            <li class="nav-item">
                                <a href="/sarpras/data-sarpras" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Sarpras</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/sarpras/laporansarpras" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Laporan Sarpras</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'sarpras')
                    <li class="nav-item">
                        <a href="/sarpras/pengajuan" class="nav-link">
                            <i class="fas fa-clipboard nav-icon"></i>
                            <p> Pengajuan Sarpras</p>
                        </a>
                    </li>
                    @endif
                    @if (Auth::user()->role == 'pelatih' || Auth::user()->role == 'atlet')
                    <li class="nav-item">
                        <a href="/{{Auth::user()->role}}/laporansarpras" class="nav-link">
                            <i class="fas fa-flag nav-icon"></i>
                            <p> Laporan Sarpras</p>
                        </a>
                    </li>
                    @endif
                    <li class="nav-item ">

                        <a class="nav-link btn-danger" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            <i class="nav-icon fa fa-sign-out"></i>
                            <p>
                                Logout
                            </p>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>