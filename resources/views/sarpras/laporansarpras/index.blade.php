@extends('layout.template')
@section('title')
    Halaman Laporan Sarpras - Sarana Prasarana
@endsection
@section('title-content')
    Data Laporan Sarpras
@endsection
@section('content')
    <div class="card-body">
        <table id="template" class="table table-bordered table-hover" >
            <thead>
                <tr >
                    <th class="text-center" width="100px">No</th>
                    <th class="text-center">Sarana Prasarana</th>
                    <th class="text-center">Tingkat Kerusakan</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Bukti</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th class="text-center" width="100px">No</th>
                    <th class="text-center">Sarana Prasarana</th>
                    <th class="text-center">Tingkat Kerusakan</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Bukti</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($laporansarpras as $key => $item)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="text-center">{{$item->sarpras->name}}</td>
                    <td class="text-center">{{$item->tingkat_kerusakan}}</td>
                    <td class="text-center">{{$item->keterangan}}</td>
                    <td class="text-center">
                        <img src="{{url('gambar/' . $item->gambar)}}" style="width:80px" /> 
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection