@extends('layout.template')
@section('title')
    Halaman Tambah Data Sarpras
@endsection
@section('title-content')
    Tambah Data Sarpras
@endsection
@section('content')
<form method="POST" action="/sarpras/data-sarpras/add-sarpras/add" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama">Nama Sarana & Prasarana</label>
        <input type="" class="form-control" name="name" id="nama" placeholder="Masukkan Nama Sarana & Prasarana">
    </div>
    <div class="form-group">
        <label for="jumlah">Jumlah Sarana Prasarana</label>
        <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Masukkan Jumlah Sarana Prasarana">
    </div>
    <div class="form-group">
        <label for="harga">Harga Sarana Prasarana</label>
        <input type="number" class="form-control" name="harga" id="harga" placeholder="Masukkan Harga Sarana Prasarana">
    </div>
    <div class="mb-3">
        <label for="gambar" class="form-label">Masukkan Gambar Sarana Prasarana</label>
        <input class="form-control" type="file" id="gambar" name="gambar">
    </div>
    <div class="form-group">
        <label for="detail">Detail Sarana & Prasarana</label>
        <input type="" class="form-control" name="detail" id="detail" placeholder="Masukkan Detail Sarana & Prasarana">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection