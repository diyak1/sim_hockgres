@extends('layout.template')
@section('title')
    Halaman Data Sarana & Prasarana
@endsection
@section('title-content')
    Data Sarpras
@endsection
@section('content')
<div class="button-action" style="margin-bottom: 20px">
<a href="/sarpras/data-sarpras/add-sarpras" class="btn btn-primary btn-md">Tambah Data</a>
</div>
<div class="card-body">
    <table id="template" class="table table-bordered table-hover" >
        <thead>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Jumlah</th>
                <th class="text-center">Harga</th>
                <th class="text-center">Gambar</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th class="text-center" width="100px">No</th>
              <th class="text-center">Nama</th>
              <th class="text-center">Jumlah</th>
              <th class="text-center">Harga</th>
              <th class="text-center">Gambar</th>
              <th class="text-center">Keterangan</th>
              <th class="text-center">Action</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($sarpras as $key => $item)
            <tr>
                <td class="text-center">{{$key+1}}</td>
                <td class="text-center">{{$item->name}}</td>
                <td class="text-center">{{$item->jumlah}}</td>
                <td class="text-center">{{formatRupiah($item->harga, true)}}</td>
                <td class="text-center">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{ $item->id }}">
                        Lihat Gambar
                      </button>
                      
                      <!-- Modal -->
                      <div class="modal fade" id="exampleModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Gambar {{ $item->name }}</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <img src="{{url('sarpras/' . $item->gambar)}}" style="width:200px" />
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                </td>
                <td class="text-center">{{$item->detail}}</td>
                <td class="text-center"><a type="button" href="/sarpras/data-sarpras/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection