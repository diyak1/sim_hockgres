@extends('layout.template')
@section('title')
    Halaman Data Pengajuan Sarpras
@endsection
@section('title-content')
    Data Pengajuan Sarana Prasarana
@endsection
@section('content')
<form method="POST" action="/sarpras/pengajuan/create">
    @csrf
    <div class="form-group">
        <label for="sarpras">Pilih Sarana Prasarana</label>
        <select class="form-control" name="id_sarpras" id="sarpras">
            @foreach ($sarpras as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Tambah Pengajuan</button>
</form>
<br>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Sarpras</th>
                <th>Pengaju</th>
                <th>Keterangan</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Penerima</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Sarpras</th>
                <th>Pengaju</th>
                <th>Keterangan</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Penerima</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pengajuan as $key => $item)
                <tr align="center">
                    <td>{{$key +1 }}</td>
                    <td>{{$item->sarpras->name}}</td>
                    <td>{{$item->pengaju->name}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{formatRupiah($item->nominal, true)}}</td>
                    <td>{{ $item->status == 0 ? "Belum di ACC" : "Sudah ACC" }}</td>
                    @if ($item->id_penerima)
                        <td>{{$item->penerima->name}}</td>
                    @else
                        <td>Belum di ACC</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection