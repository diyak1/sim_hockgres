@extends('layout.template')
@section('title')
    Halaman Form Pengajuan Sarpras
@endsection
@section('title-content')
    Form Pengajuan Sarpras
@endsection
@section('content')
<form action="/sarpras/pengajuan/store" method="POST">
    @csrf
    <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <input type="" class="form-control" name="keterangan" id="keterangan" placeholder="Masukkan keterangan pengajuan">
      </div>
      <div class="form-group">
        <label for="nominal">Nominal</label>
        <input type="" class="form-control" name="nominal" id="nominal" placeholder="Masukkan nominal pengajuan">
    </div>
    <div class="form-group" style="display: none">
        <label for="idsarpras">idsarpras</label>
        <input type="" class="form-control" name="sarpras" value="{{$idsarpras}}" id="sarpras" value="{{$idsarpras}}" placeholder="Masukkan nominal pengajuan">
    </div>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Sarpras</th>
                    <th>Pelapor</th>
                    <th>Tingkat Kerusakan</th>
                    <th>Keterangan</th>
                    <th>Pilih</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Nama Sarpras</th>
                    <th>Pelapor</th>
                    <th>Tingkat Kerusakan</th>
                    <th>Keterangan</th>
                    <th>Pilih</th>
                </tr>
            </tfoot>
            <tbody>
                @php
                    $nomer = 0;
                @endphp
                @foreach ($laporan as $key => $item)
                    <tr align="center">
                        <td>{{$nomer+1}}</td>
                        <td>{{$item->sarpras->name}}</td>
                        <td>{{$item->pelapor->name}}</td>
                        <td>{{$item->tingkat_kerusakan}}</td>
                        <td>{{$item->keterangan}}</td>
                        <td><input type="checkbox" name="idlaporan[]" value="{{$item->id}}"></td> 
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <button type="submit" class="btn btn-primary mb-3">Buat Pengajuan</button>
</form>
@endsection