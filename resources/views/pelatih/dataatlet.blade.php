@extends('layout.template')
@section('title')
    Halaman Akun Atlet
@endsection
@section('title-content')
    Data Atlet
@endsection
@section('content')
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="all-tab" data-toggle="tab" data-target="#all" type="button" role="tab" aria-controls="home" aria-selected="true">All</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="false">Indoor Putra</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Indoor Putri</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="contact-tab" data-toggle="tab" data-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Outdoor Putra</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="outdoor-tab" data-toggle="tab" data-target="#outdoor" type="button" role="tab" aria-controls="outdoor" aria-selected="false">Outdoor Putri</button>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
        <div class="card-body">
            <table id="template" class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Jenis Kelamin</th>
                        <th class="text-center">Umur</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center">Kategori</th>
                        </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Jenis Kelamin</th>
                        <th class="text-center"h>Umur</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center">Kategori</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($atlet as $key => $item)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">{{Str::title($item->name)}}</td>
                            <td class="text-center">{{$item->jenis_kelamin}}</td>
                            <td class="text-center">{{$item->umur}}</td>
                            <td class="text-center">{{$item->alamat}}</td>
                            <td class="text-center">{{$item->kategori->kategori}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table> 
        </div>
    </div>
    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer=0;
                    @endphp
                    @foreach ($indoorputra as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{Str::title($item->name)}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer=0;
                    @endphp
                    @foreach ($indoorputri as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{Str::title($item->name)}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer=0;
                    @endphp
                    @foreach ($outdoorputra as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{Str::title($item->name)}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="outdoor" role="tabpanel" aria-labelledby="outdoor-tab">
        <div class="card-body">
            <table id="template" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer=0;
                    @endphp
                    @foreach ($outdoorputri as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{Str::title($item->name)}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
@endsection
