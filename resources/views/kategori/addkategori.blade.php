@extends('layout.template')
@section('title')
    Halaman Kategori
@endsection
@section('title-content')
    Tambah Kategori
@endsection
@section('content')
<form method="POST" action="/kategori/add">
    @csrf
    <div class="form-group">
        <label for="kategori">Kategori</label>
        <select class="form-control" name="kategori" id="kategori">
          <option value="Hockey Indoor Putra">Hockey Indoor Putra</option>
          <option value="Hockey Indoor Putri">Hockey Indoor Putri</option>
          <option value="Hockey outdoor Putra">Hockey Outdoor Putra</option>
          <option value="Hockey outdoor Putri">Hockey Outdoor Putri</option>
        </select>
      </div>
      <div class="form-group">
        <label for="gender">Gender</label>
        <select class="form-control" name="gender" id="gender">
          <option value="0">Laki - Laki</option>
          <option value="1">Perempuan</option>
        </select>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection