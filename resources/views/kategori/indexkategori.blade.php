@extends('layout.template')
@section('title')
    Halaman Kategori
@endsection
@section('title-content')
    Daftar Kategori
@endsection
@section('content')
<a href="/kategori/addkategori" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Kategori</a>
<div class="card-body">
    <table id="template" class="table table-bordered table-hover" >
        <thead>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Kategori</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Kategori</th>
                <th class="text-center">Action</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($kategori as $key => $item)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="text-center">{{$item->kategori}}</td>
                    <td class="text-center"><a type="button" href="/kategori/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection