@extends('layout.template')
@section('title')
    Halaman Pencairan gaji
@endsection
@section('title-content')
    Pencairan gaji
@endsection
@section('content')
<div class="card-body">
    <table id="example1" class="table table-bordered table-hover" >
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Atlet</th>
                <th>Nominal</th>
                <th>Bulan</th>
                <th>Presentasi Absen</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Nama Atlet</th>
                <th>Nominal</th>
                <th>Bulan</th>
                <th>Presentasi Absen</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($pencairan as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->atlet->name}}</td>
                    <td>{{$item->nominal}}</td>
                    <td>{{$item->bulan}}</td>
                    <td>{{$item->presentase}}%</td>
                    @if ($item->tanggal_pencairan == null)
                        <td>
                            <a type="button" href="/keuangan/pencairan/{{ $item->id }}" class="btn btn-danger m-1">Cairkan Gaji</a>
                        </td>
                    @else
                        <td>{{date('d-M-Y', strtotime($item->tanggal_pencairan))}}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection