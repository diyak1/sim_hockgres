@extends('layout.template')
@section('title')
    Halaman Pilih Kategori
@endsection
@section('title-content')
    Pilih Kategori
@endsection
@section('content')
<form method="POST" action="/keuangan/setting_gaji/tambah">
    @csrf
    <div class="form-group">
        <label for="kategori">Kategori Cabor</label>
        <select class="form-control" name="id_kategori" id="kategori">
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->kategori}}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection