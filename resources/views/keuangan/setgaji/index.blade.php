@extends('layout.template')
@section('title')
    Halaman Setting Gaji
@endsection
@section('title-content')
    Setting Gaji
@endsection
@section('content')
<a href="/keuangan/setting_gaji/add-kategori" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Kategori</a>
<div class="card-body">
    <table id="example1" class="table table-bordered table-hover" >
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Kategori</th>
                <th>Nominal</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Kategori</th>
                <th>Nominal</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($setGaji as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->kategori->kategori}}</td>
                    <form action="/keuangan/editsetgaji/{{ $item->id }}" method="POST"> 
                        @csrf
                        <td>
                            <input type="number" name="nominal" value="{{ $item->nominal }}">
                        </td>
                        <td>
                            <button type="submit" class="btn btn-success m-1">Edit Nominal</button>
                        </td>
                    </form>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection