@extends('layout.template')
@section('title')
    Halaman form generate gaji
@endsection
@section('title-content')
    form generate gaji
@endsection
@section('content')
<div class="card-body">
    <form action="/keuangan/generategaji/actionadd" method="POST">
        @csrf
        <div class="form-group">
            <label for="bulan">Bulan</label>
            <input type="month" class="form-control" name="bulan" id="bulan">
        </div>
        <input type="text" name="id_kategori" value="{{ $kategori->id }}" style="display: none">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection