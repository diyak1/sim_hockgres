@extends('layout.template')
@section('title')
    Halaman Generate Gaji
@endsection
@section('title-content')
    Generate Gaji
@endsection
@section('content')
<div class="card-body">
    <table id="example1" class="table table-bordered table-hover" >
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Kategori</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Kategori</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($generate as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->kategori}}</td>
                    <td><a type="button" href="/keuangan/generategaji/add_generategaji/{{ $item->id }}" class="btn btn-danger m-1">Generate Gaji</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection