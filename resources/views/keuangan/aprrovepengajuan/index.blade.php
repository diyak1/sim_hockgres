@extends('layout.template')
@section('title')
    Halaman Approve Pengajuan
@endsection
@section('title-content')
    Data Pengajuan
@endsection
@section('content')
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr align="center">
                <th>No</th>
                <th>Sarpras</th>
                <th>Pengaju</th>
                <th>Keterangan</th>
                <th>Laporan Terlampir</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Penerima</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Sarpras</th>
                <th>Pengaju</th>
                <th>Keterangan</th>
                <th>Laporan Terlampir</th>
                <th>Nominal</th>
                <th>Status</th>
                <th>Penerima</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($pengajuan as $key => $item)
                <tr align="center">
                    <td>{{$key +1 }}</td>
                    <td>{{$item->sarpras->name}}</td>
                    <td>{{$item->pengaju->name}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$key}}">
                            Lihat Laporan
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content"> 
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Laporan Terlampir</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                @php
                                    $detail = App\Models\PengajuanDetail::where('id_pengajuan',$item->id)->get();
                                @endphp
                                <div class="modal-body">
                                    <table class="table">
                                        <thead>
                                          <tr align="center">
                                            <th scope="col">Sarpras</th>
                                            <th scope="col">Pelapor</th>
                                            <th scope="col">T.Kerusakan</th>
                                            <th scope="col">Keterangan</th>
                                            <th scope="col">Bukti</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($detail as $value)
                                                <tr align="center">
                                                    <td>{{$value->laporan->sarpras->name}}</td>
                                                    <td>{{$value->laporan->pelapor->name}}</td>
                                                    <td>{{$value->laporan->tingkat_kerusakan}}</td>
                                                    <td>{{$value->laporan->keterangan}}</td>
                                                    <td>
                                                        <img src="{{url('gambar/' . $value->laporan->gambar)}}" style="width:75px" /> 
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </td>           
                    <td>{{formatRupiah($item->nominal, true)}}</td>
                    @if ($item->status == 0)
                        <td><a href="/keuangan/pengajuan-sarpras/acc/{{$item->id}}" class="btn btn-success mx-1">ACC</a><a href="/keuangan/pengajuan-sarpras/reject/{{$item->id}}" class="btn btn-danger">Tolak</a></td>
                    @elseif ($item->status == 1)
                       <td>Sudah di ACC</td> 
                    @elseif ($item->status == 2)
                        <td>Ditolak</td>
                    @endif
                    @if ($item->status == 0)
                        <td>Belum di ACC</td>
                    @elseif ($item->status == 1)
                       <td>{{$item->penerima->name}}</td> 
                    @elseif ($item->status == 2)
                        <td>-</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection