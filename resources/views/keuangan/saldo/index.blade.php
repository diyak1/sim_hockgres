@extends('layout.template')
@section('title')
    Halaman Data Keuangan
@endsection
@section('title-content')
    Data Keuangan
@endsection
@section('content')
<a href="/keuangan/tambah-data-keuangan" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Saldo</a>
<div class="alert alert-danger" role="alert">
    Total Saldo Sekarang = {{formatRupiah($saldo, true)}}
</div>
<div class="card-body">
    <table id="template" class="table table-bordered table-hover" >
        <thead>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Tipe</th>
                <th class="text-center">Nominal</th>
                <th class="text-center">Keterangan</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Tipe</th>
                <th class="text-center">Nominal</th>
                <th class="text-center">Keterangan</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($keuangan as $key => $item)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    @if ($item->tipe == 0)
                        <td class="text-center">Uang Keluar</td> 
                    @elseif ($item->tipe == 1)
                        <td class="text-center">Uang Masuk</td>
                    @endif
                    <td class="text-center">{{formatrupiah($item->nominal, true)}}</td>
                    <td class="text-center">{{$item->keterangan}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection