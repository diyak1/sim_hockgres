@extends('layout.template')
@section('title')
    Halaman Tambah Data Keuangan
@endsection
@section('title-content')
    Tambah Data keuangan
@endsection
@section('content')
<form method="POST" action="/keuangan/data-keuangan/add" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="tipe">Tipe</label>
        <select class="form-control" name="tipe" id="tipe">
            <option value="0">Keluar</option>
            <option value="1">Masuk</option>
        </select>
    </div>
    <div class="form-group">
        <label for="nominal">Nominal</label>
        <input type="" class="form-control" name="nominal" id="nominal" placeholder="Masukkan Nominal">
    </div>
    <div class="form-group">
        <label for="keterangan">Keteranga </label>
        <input type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Masukkan Keterangan">
    </div>
     <br>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection