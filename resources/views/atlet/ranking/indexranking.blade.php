@extends('layout.template')
@section('title')
    Halaman Pilih Jadkom
@endsection
@section('title-content')
    Form Pilih Jadkommm
@endsection
@section('content')
<div class="card-body">
    <table class="table table-bordered table-striped" >
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Kompetisi</th>
                <th>Kategori</th>
                <th>Waktu</th>
                <th>Tempat</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Nama Kompetisi</th>
                <th>Kategori</th>
                <th>Waktu</th>
                <th>Tempat</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($jadkom as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->kategori->kategori}}</td>
                    <td>{{date('d-M-Y', strtotime ($item->waktu))}}</td>
                    <td>{{ $item->tempat }}</td>
                    <td><a type="button" href="/atlet/hasil-seleksi/{{ $item->id }}" class="btn btn-warning m-1">Pilih</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection