@extends('layout.template')
@section('title')
    Halaman Absensi Atlet
@endsection
@section('title-content')
    Data Absensi Atlet - {{ $atlet->name }}
@endsection
@section('content')
<div class="card-body">
    <table class="table table-bordered table-striped" >
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Atlet</th>
                <th>Latihan</th>
                <th>Bulan</th>
                <th>Status</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Nama Atlet</th>
                <th>Latihan</th>
                <th>Bulan</th>
                <th>Status</th>
                <th>Keterangan</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($absensi as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->atlet->name}}</td>
                    <td>{{$item->jadlat->name}}</td>
                    <td>{{$item->bulan}}</td>
                    @if ($item->status == 1)
                        <td>Hadir</td> 
                    @elseif ($item->status == 2)
                        <td>Izin</td>
                    @elseif ($item->status == 3)
                        <td>Sakit</td>
                    @elseif ($item->status == 4)
                        <td>Alpa</td>
                    @elseif ($item->status == 0)
                        <td>Belum di isi</td>
                    @endif
                    <td>{{$item->keterangan}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection