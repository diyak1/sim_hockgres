@extends('layout.template')
@section('title')
    Halaman Data Penilaian Atlet
@endsection
@section('title-content')
    Data Penilaian Atlet - {{Str::title($atlet->name)}} - {{ $jadkom->name }}
@endsection
@section('content')
<div class="card-body">
    <table class="table table-bordered table-striped" id="">
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Atlet</th>
                <th>Kategori</th>
                <th>Waktu Penilaian</th>
                <th>Kriteria</th>
                <th>Sub-Kriteria</th>
                <th>Nilai Profil</th>
                <th>Nilai Standar Profil</th>
                <th>Gap</th>
                <th>Nilai Gap</th>
                <th>Rata - Rata</th>
                <th>Total Nilai</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Nama Atlet</th>
                <th>Kategori</th>
                <th>Waktu Penilaian</th>
                <th>Kriteria</th>
                <th>Sub-Kriteria</th>
                <th>Nilai Profil</th>
                <th>Nilai Standar Profil</th>
                <th>Gap</th>
                <th>Nilai Gap</th>
                <th>Rata - Rata</th>
                <th>Total Nilai</th>
            </tr>
        </tfoot>
        <tbody align="center">
           
                <tr>
                    @php
                    if (!empty($result['nilai'])) {
                        $count = (count($result['nilai']));
                    }else {
                        $count =1 ;
                    }
                    if (!empty($result['total'])) {
                        $countncf = (count(App\Models\Kriteria::where('jenis',1)->get()));
                        $countnsf = (count(App\Models\Kriteria::where('jenis',2)->get()));
                    }else {
                        $countncf =1 ;
                        $countnsf =1;
                    }
                    @endphp
                    <td rowspan="{{$count}}">1</td>
                    <td rowspan="{{$count}}">{{$result['atlet']->name}}</td>
                    <td rowspan="{{$count}}">{{$result['atlet']->kategori->kategori}}</td>
                    @if (!empty($result['nilai']))   
                        <td>{{ date('d-M-Y', strtotime ($result['nilai'][0]->created_at ))}}</td>
                        <td>{{ $result['nilai'][0]->kriteria->name }}</td>
                        <td>{{ $result['nilai'][0]->subkriteria->name }}</td>
                        <td>{{ $result['nilai'][0]->subkriteria->nilai }}</td>
                        <td>{{ $result['nilai'][0]->kriteria->nilai_standar }}</td>
                        <td>{{ $result['nilai'][0]->subkriteria->nilai - $result['nilai'][0]->kriteria->nilai_standar }}</td> 
                        <td>{{ $result['nilai'][0]->gap }}</td>
                        <td rowspan="{{ $countncf }}">{{ $result['total'][0]->ncf }}</td>
                        <td rowspan="{{$count}}">{{$result['total'][0]->total}}</td>

                    @else
                        <td colspan="9">Nilai Belum di Isi</td>
                    @endif
                    {{-- <td rowspan="{{$count}}">{{$item->kategori->kategori}}</td> --}}

                </tr>
                @if (!empty($result['nilai']))    
                    @for ($i = 1; $i < $count; $i++)
                    <tr>
                        <td>{{ date('d-M-Y', strtotime ($result['nilai'][$i]->created_at)) }}</td>
                        <td>{{ $result['nilai'][$i]->kriteria->name }}</td>
                        <td>{{ $result['nilai'][$i]->subkriteria->name }}</td>
                        <td>{{ $result['nilai'][$i]->subkriteria->nilai }}</td>
                        <td>{{ $result['nilai'][$i]->kriteria->nilai_standar }}</td>
                        <td>{{ $result['nilai'][$i]->subkriteria->nilai - $result['nilai'][$i]->kriteria->nilai_standar }}</td>
                        <td>{{ $result['nilai'][$i]->gap }}</td>
                        @if ($i == $countncf)
                        <td rowspan="{{ $countnsf }}">{{ $result['total'][0]->nsf }}</td> 
                        @endif
                    </tr>
                    @endfor
                @endif
        </tbody>
    </table>
</div>
@endsection