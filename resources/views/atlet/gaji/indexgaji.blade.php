@extends('layout.template')
@section('title')
    Halaman Gaji Atlet
@endsection
@section('title-content')
    Data Gaji Atlet - {{ $atlet->name }}
@endsection
@section('content')
<div class="card-body">
    <table class="table table-bordered table-striped" >
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Atlet</th>
                <th>Bulan</th>
                <th>Nominal</th>
                <th>Tanggal Pencairan</th>
                <th>Presentase Absensi</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Nama Atlet</th>
                <th>Bulan</th>
                <th>Nominal</th>
                <th>Tanggal Pencairan</th>
                <th>Presentase Absensi</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($gaji as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->atlet->name}}</td>
                    <td>{{$item->bulan}}</td>
                    <td>{{formatRupiah($item->nominal, true)}}</td>
                    @if ($item->tanggal_pencairan == null)
                    <td>
                        <p>Belum Dicairkan</p>
                    </td>
                @else
                    <td>{{date('d-M-Y', strtotime($item->tanggal_pencairan))}}</td>
                @endif
                    <td>{{$item->presentase}}%</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection