@extends('layout.template')
@section('title')
    Halaman Jadwal Kompetisi
@endsection
@section('title-content')
    Jadwal Kompetisi
@endsection
@section('content')
@if (Auth::user()->role == 'manajer')
<a href="/manajer/jadwal/jadkom/add-datajadkom" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Jadwal Kompetisi</a>
@endif
<div class="card-body">
    <table class="table table-bordered table-striped" >
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Kompetisi</th>
                <th>Kategori</th>
                <th>Waktu</th>
                <th>Tempat</th>
                <th>Rincian</th>
                @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Nama Kompetisi</th>
                <th>Kategori</th>
                <th>Waktu</th>
                <th>Tempat</th>
                <th>Rincian</th>
                @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($jadkom as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->kategori->kategori}}</td>
                    <td>{{date('d-M-Y', strtotime ($item->waktu))}}</td>
                    <td>{{ $item->tempat }}</td>
                    <td><a href="{{ url('/'.Auth::user()->role.'/download',$item->file) }}">Download</a></td>
                    @if (Auth::user()->role == 'manajer')<td><a type="button" href="/manajer/jadwal/jadkom/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>@endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection