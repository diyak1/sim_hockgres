@extends('layout.template')
@section('title')
    Halaman Jadwal Kompetisi
@endsection
@section('title-content')
    Tambah Jadwal Kompetisi
@endsection
@section('content')
<form method="POST" action="/manajer/jadwal/jadkom/add-jadkom" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="namakompetisi">Nama Kompetisi</label>
        <input type="" class="form-control" name="name" id="namakompetisi" placeholder="Masukkan Nama Kompetisi">
    </div>
    <div class="form-group">
      <label for="kategori">Kategori</label>
      <select class="form-control" name="id_kategori" id="kategori">
          @foreach ($kategori as $item)
              <option value="{{$item->id}}">{{$item->kategori}}</option>
          @endforeach
      </select>
  </div>
    <div class="form-group">
      <label for="waktu">Waktu</label>
      <input type="date" class="form-control" name="waktu" id="waktu" placeholder="Masukkan waktu">
    </div>
    <div class="form-group">
      <label for="tempat">Tempat</label>
      <input type="" class="form-control" name="tempat" id="tempat" placeholder="Masukkan Tempat Kompetisi">
    </div>
    <div class="mb-3">
      <label for="file" class="form-label">Masukkan Rincian</label>
      <input class="form-control" type="file" id="rincian" name="file">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection