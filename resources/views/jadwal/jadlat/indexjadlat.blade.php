@extends('layout.template')
@section('title')
    Halaman Jadwal Latihan
@endsection
@section('title-content')
    Jadwal Latihan
@endsection
@section('content')
@if (Auth::user()->role == 'pelatih')
<a href="/pelatih/jadwal/jadlat/add-datajadlat" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Jadwal Latihan</a>
@endif
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="all-tab" data-toggle="tab" data-target="#all" type="button" role="tab" aria-controls="home" aria-selected="true">All</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="false">Indoor Putra</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Indoor Putri</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="contact-tab" data-toggle="tab" data-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Outdoor Putra</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="outdoor-tab" data-toggle="tab" data-target="#outdoor" type="button" role="tab" aria-controls="outdoor" aria-selected="false">Outdoor Putri</button>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
        <div class="card-body">
            <table id="template" class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Kategori</th>
                        <th class="text-center">Waktu Mulai</th>
                        <th class="text-center">Waktu Akhir</th>
                        <th class="text-center">Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th class="text-center">Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Kategori</th>
                        <th class="text-center">Waktu Mulai</th>
                        <th class="text-center">Waktu Akhir</th>
                        <th class="text-center">Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th class="text-center">Action</th>@endif
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($jadlat as $key => $item)
                    <tr>
                        <td class="text-center">{{$key+1}}</td>
                        <td class="text-center">{{Str::title($item->name)}}</td>
                        <td class="text-center">{{$item->kategori->kategori}}</td>
                        <td class="text-center">{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                        <td class="text-center">{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                        <td class="text-center">{{$item->deskripsi}}</td>
                        @if (Auth::user()->role == 'pelatih')
                        <td class="text-center"><a type="button" href="/pelatih/jadwal/jadlat/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="card-body">
            <table id="tabel1" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                    $nomer=0;
                @endphp
                    @foreach ($jadlatindoorputra as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                            <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                            <td>{{$item->deskripsi}}</td>
                            @if (Auth::user()->role == 'manajer')
                            <td><a type="button" href="/pelatih/jadwal/jadlat/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                            @endif
                        </tr>
                    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card-body">
            <table id="tabel3" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                    $nomer=0;
                @endphp
                    @foreach ($jadlatindoorputri as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                            <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                            <td>{{$item->deskripsi}}</td> 
                            @if (Auth::user()->role == 'manajer')
                            <td><a type="button" href="/pelatih/jadwal/jadlat/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                            @endif
                        </tr>
                    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <div class="card-body">
            <table id="tabel5" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                    $nomer=0;
                @endphp
                    @foreach ($jadlatoutdoorputra as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                            <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                            <td>{{$item->deskripsi}}</td>
                            @if (Auth::user()->role == 'manajer')
                            <td><a type="button" href="/pelatih/jadwal/jadlat/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                            @endif
                        </tr>
                    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="outdoor" role="tabpanel" aria-labelledby="outdoor-tab">
        <div class="card-body">
            <table id="tabel7" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'manajer')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                    $nomer=0;
                @endphp
                    @foreach ($jadlatoutdoorputri as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                            <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                            <td>{{$item->deskripsi}}</td>
                            @if (Auth::user()->role == 'manajer')
                            <td><a type="button" href="/pelatih/jadwal/jadlat/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                            @endif
                        </tr>
                    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
  @endsection
