@extends('layout.template')
@section('title')
    Halaman Jadwal Kompetisi
@endsection
@section('title-content')
    Tambah Jadwal Kompetisi
@endsection
@section('content')
<form method="POST" action="/pelatih/jadwal/jadlat/add-jadlat" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="namalatihan">Nama Latihan</label>
        <input type="" class="form-control" name="name" id="namalatihan" placeholder="Masukkan Nama Latihan">
    </div>
    <div class="form-group">
        <label for="kategori">Kategori Cabor</label>
        <select class="form-control" name="id_kategori" id="kategori">
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->kategori}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="start">Waktu Mulai</label>
        <input type="datetime-local" class="form-control" name="start" id="start" placeholder="Masukkan waktu Mulai" min="{{Carbon\Carbon::now()->firstOfYear()->format('Y-m-d')}}" max="{{Carbon\Carbon::now()->lastOfYear()->format('Y-m-d')}}">
    </div>
    <div class="form-group">
      <label for="end">Waktu Akhir</label>
      <input type="datetime-local" class="form-control" name="end" id="end" placeholder="Masukkan waktu Akhir" min="{{Carbon\Carbon::now()->firstOfYear()->format('Y-m-d')}}" max="{{Carbon\Carbon::now()->lastOfYear()->format('Y-m-d')}}">
  </div>
  <div class="form-group">
    <label for="deskripsi">Deskripsi</label>
    <input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi">
</div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection