@extends('layout.template')
@section('title')
    Halaman Generate Absensi Atlet
@endsection
@section('title-content')
    Generate Absensi Atlet
@endsection
@section('content')
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="all-tab" data-toggle="tab" data-target="#all" type="button" role="tab" aria-controls="home" aria-selected="true">All</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="false">Indoor Putra</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Indoor Putri</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="contact-tab" data-toggle="tab" data-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Outdoor Putra</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="outdoor-tab" data-toggle="tab" data-target="#outdoor" type="button" role="tab" aria-controls="outdoor" aria-selected="false">Outdoor Putri</button>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @foreach ($jadlat as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->kategori->kategori}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                        <td>{{$item->deskripsi}}</td>
                        @if (Auth::user()->role == 'pelatih')
                        @if ($item->status == 1)
                            <td>Sudah digenerate</td>
                        @else
                           <td><a type="button" href="/pelatih/generateabsensi/{{$item->id}}" class="btn btn-danger m-1">Generate Absensi</a></td>
                        @endif
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer=0;
                    @endphp
                    @foreach ($jadlatindoorputra as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->kategori->kategori}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                        <td>{{$item->deskripsi}}</td>
                        @if (Auth::user()->role == 'pelatih')
                        @if ($item->status == 1)
                            <td>Sudah digenerate</td>
                        @else
                           <td><a type="button" href="/pelatih/generateabsensi/{{$item->id}}" class="btn btn-danger m-1">Generate Absensi</a></td>
                        @endif
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer=0;
                    @endphp
                    @foreach ($jadlatindoorputri as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->kategori->kategori}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                        <td>{{$item->deskripsi}}</td>
                        @if (Auth::user()->role == 'pelatih')
                        @if ($item->status == 1)
                            <td>Sudah digenerate</td>
                        @else
                           <td><a type="button" href="/pelatih/generateabsensi/{{$item->id}}" class="btn btn-danger m-1">Generate Absensi</a></td>
                        @endif
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer=0;
                    @endphp
                    @foreach ($jadlatoutdoorputra as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->kategori->kategori}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                        <td>{{$item->deskripsi}}</td>
                        @if (Auth::user()->role == 'pelatih')
                        @if ($item->status == 1)
                            <td>Sudah digenerate</td>
                        @else
                           <td><a type="button" href="/pelatih/generateabsensi/{{$item->id}}" class="btn btn-danger m-1">Generate Absensi</a></td>
                        @endif
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="outdoor" role="tabpanel" aria-labelledby="outdoor-tab">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Akhir</th>
                        <th>Deskripsi</th>
                        @if (Auth::user()->role == 'pelatih')<th>Action</th>@endif
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer=0;
                    @endphp
                    @foreach ($jadlatoutdoorputri as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->kategori->kategori}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->start))}}</td>
                        <td>{{date('d-M-Y | H:i:s', strtotime ($item->end))}}</td>
                        <td>{{$item->deskripsi}}</td>
                        @if (Auth::user()->role == 'pelatih')
                        @if ($item->status == 1)
                            <td>Sudah digenerate</td>
                        @else
                           <td><a type="button" href="/pelatih/generateabsensi/{{$item->id}}" class="btn btn-danger m-1">Generate Absensi</a></td>
                        @endif
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
@endsection