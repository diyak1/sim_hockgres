@extends('layout.template')
@section('title')
    Halaman Isi Absensi Atlet
@endsection
@section('title-content')
    Isi Absensi Atlet
@endsection
@section('content')
<div class="card-body">
    <form action="/pelatih/absensi/add" method="post">
        @csrf
        <input type="" name="id_jadlat" value="{{ $jadlat->id }}" style="display: none">
        <table id="template" class="table table-bordered table-hover" >
            <thead>
                <tr>
                    <th class="text-center" width="100px">No</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Kategori</th>
                    <th class="text-center">Absen</th>
                    <th class="text-center">Keterangan</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th class="text-center" width="100px">No</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Kategori</th>
                    <th class="text-center">Absen</th>
                    <th class="text-center">Keterangan</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach ($absensi as $key => $item)
                    <tr>
                        <td class="text-center">{{$key+1}}</td>
                        <td class="text-center">{{$item->atlet->name}}</td>
                        <td class="text-center">{{$item->atlet->kategori->kategori}}</td>
                        <td class="text-center">
                            <input type="text" name="id[]" value="{{ $item->atlet->id }}" style="display: none">
                            <select name="absensi[]" id="absensi">
                                <option value="1">Hadir</option>
                                <option value="2">Izin</option>
                                <option value="3">Sakit</option>
                                <option value="4">Alpa</option>
                              </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="keterangan[]" id="keterangan" placeholder="Masukkan Keterangan">
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection