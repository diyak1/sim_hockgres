@extends('layout.template')
@section('title')
    Halaman Data Absensi Atlet
@endsection
@section('title-content')
    Data Absensi Atlet - {{ $jadlat->name }} Kategori {{ $jadlat->kategori->kategori }}
@endsection
@section('content')
<div class="card-body">
    <table id="example1" class="table table-bordered table-hover" >
        <thead>
            <th class="text-center" width="100px">No</th>
            <th class="text-center">Nama Atlet</th>
            <th class="text-center">Latihan</th>
            <th class="text-center">Bulan</th>
            <th class="text-center">Status</th>
            <th class="text-center">Keterangan</th>
        </thead>
        <tfoot>
            <th class="text-center" width="100px">No</th>
            <th class="text-center">Nama Atlet</th>
            <th class="text-center">Latihan</th>
            <th class="text-center">Bulan</th>
            <th class="text-center">Status</th>
            <th class="text-center">Keterangan</th>
        </tfoot>
        <tbody align="center">
            @foreach ($absensi as $key => $item)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="text-center">{{$item->atlet->name}}</td>
                    <td class="text-center">{{$item->jadlat->name}}</td>
                    <td class="text-center">{{$item->bulan}}</td>
                    @if ($item->status == 1)
                        <td class="text-center">Hadir</td> 
                    @elseif ($item->status == 2)
                        <td class="text-center">Izin</td>
                    @elseif ($item->status == 3)
                        <td class="text-center">Sakit</td>
                    @elseif ($item->status == 4)
                        <td class="text-center">Alpa</td>
                    @elseif ($item->status == 0)
                        <td class="text-center">Belum di isi</td>
                    @endif
                    <td class="text-center">{{$item->keterangan}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection