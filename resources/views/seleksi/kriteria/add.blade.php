@extends('layout.template')
@section('title')
    Halaman Kategori Data Kriteria
@endsection
@section('title-content')
    Pilih Kategori
@endsection
@section('content')
<form method="POST" action="/pelatih/kriteria/add" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="namakriteria">Nama Kriteria</label>
        <input type="" class="form-control" name="name" id="namakriteria" placeholder="Masukkan Nama Kriteria">
    </div>
    <div>
        <label for="jeniskriteria">Jenis Kriteria</label>
        <select class="form-control" name="jenis" id="jeniskriteria" >
            <option value="1">Core Factor</option>
            <option value="2">Secondary Factor</option>
          </select>
    </div>
    <div>
        <label for="nilaistandar">Nilai Standar</label>
        <select class="form-control" name="nilai_standar" id="nilaistandar">
            <option value="1">1 (Sangat Kurang)</option>
            <option value="2">2 (Kurang)</option>
            <option value="3">3 (Cukup)</option>
            <option value="4">4 (Baik)</option>
            <option value="5">5 (Sangat Baik)</option>
          </select>
    </div> <br>
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection