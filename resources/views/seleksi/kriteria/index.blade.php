@extends('layout.template')
@section('title')
    Halaman Kriteria
@endsection
@section('title-content')
    Data Kriteria
@endsection
@section('content')
<!-- <a href="/pelatih/pilih-kategori-kriteria" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Kriteria</a> -->
<div class="button-action" style="margin-bottom: 20px">
    <a href="/pelatih/pilih-kategori-kriteria" class="btn btn-primary">Tambah Data Kriteria</a>
</div>

<div class="card-body">
    <table id="template" class="table table-bordered table-hover" >
        <thead>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Nama Kriteria</th>
                <th class="text-center">Jenis Kriteria</th>
                <th class="text-center">Nilai Standar</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Nama Kriteria</th>
                <th class="text-center">Nilai Standar</th>
                <th class="text-center">Jenis Kriteria</th>
                <th class="text-center">Action</th>
            </tr>
        </tfoot>
        <tbody >
            @foreach ($kriteria as $key => $item)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="text-center">{{$item->name}}</td>
                    @if ($item->jenis == 0)
                        <td class="text-center">Belum Di isi</td> 
                    @elseif ($item->jenis == 1)
                        <td class="text-center">Core Factor</td>
                    @elseif ($item->jenis == 2)
                        <td class="text-center">Secondary Factor</td>
                    @endif
                    <td class="text-center">{{ $item->nilai_standar }}</td>
                    <td class="text-center">
                        <a type="button" href="/pelatih/kriteria/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="modal fade" id="import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">IMPORT DATA KRITERIA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/pelatih/kriteria/import-excel" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>PILIH FILE</label>
                        <input type="file" name="file" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
                    <button type="submit" class="btn btn-success">IMPORT</button>
                </div>
            </form>
        </div>
    </div>
  </div>
@endsection