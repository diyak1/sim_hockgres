@extends('layout.template')
@section('title')
    Halaman Penilaian Atlet
@endsection
@section('title-content')
    Data Penilaian Atlet
@endsection
@section('content')
<a href="/pelatih/form-jadkom" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Penilaian</a>
<div class="card-body">
    <table id="template" class="table table-bordered table-hover" >
        <thead>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Nama Atlet</th>
                <th class="text-center">Kategori</th>
                <th class="text-center">Persiapan</th>
                <th class="text-center">Waktu Penilaian</th>
                <th class="text-center">Kriteria</th>
                <th class="text-center">Sub Kriteria</th>
                <th class="text-center">Nilai Profil</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Nama Atlet</th>
                <th class="text-center">Kategori</th>
                <th class="text-center">Persiapan</th>
                <th class="text-center">Waktu Penilaian</th>
                <th class="text-center">Kriteria</th>
                <th class="text-center">Sub Kriteria</th>
                <th class="text-center">Nilai Profil</th>
                <th class="text-center">Action</th>
            </tr>
        </tfoot>
        <tbody >
            {{-- @foreach ($kriteria as $key => $item)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="text-center">{{$item->name}}</td>
                    @if ($item->jenis == 0)
                        <td class="text-center">Belum Di isi</td> 
                    @elseif ($item->jenis == 1)
                        <td class="text-center">Core Factor</td>
                    @elseif ($item->jenis == 2)
                        <td class="text-center">Secondary Factor</td>
                    @endif
                    <td class="text-center">
                        <a type="button" href="" class="btn btn-success m-1">Edit</a>
                        <a type="button" href="/pelatih/kriteria/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a>
                    </td>
                </tr>
            @endforeach --}}
        </tbody>
    </table>
</div>
@endsection