@extends('layout.template')
@section('title')
    Halaman Form Pilih Atlet
@endsection
@section('title-content')
    Form Pilih Atlet
@endsection
@section('content')
<div class="card-body">
    <table class="table table-bordered table-striped" >
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Atlet</th>
                <th>Kategori</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Nama Kompetisi</th>
                <th>Kategori</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($atlet as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->kategori->kategori}}</td>
                    <td><a type="button" href="/pelatih/form-atlet/{{ $jadkom->id }}/{{ $item->id }}" class="btn btn-warning m-1">Nilai</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection