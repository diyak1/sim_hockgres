@extends('layout.template')
@section('title')
    Halaman Detail Penilaian
@endsection
@section('title-content')
    Data Penilaian Atlet
@endsection
@section('content')
<div class="card-body">
    <table class="table table-bordered table-striped" id="">
        <thead>
            <tr align="center">
                <th width="100px">No</th>
                <th>Nama Atlet</th>
                <th>Kategori</th>
                <th>Waktu Penilaian</th>
                <th>Kriteria</th>
                <th>Sub-Kriteria</th>
                <th>Nilai Profil</th>
                <th>Nilai Standar Profil</th>
                <th>Gap</th>
                <th>Nilai Gap</th>
                <th>Rata - Rata</th>
                <th>Total Nilai</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th>No</th>
                <th>Nama Atlet</th>
                <th>Kategori</th>
                <th>Waktu Penilaian</th>
                <th>Kriteria</th>
                <th>Sub-Kriteria</th>
                <th>Nilai Profil</th>
                <th>Nilai Standar Profil</th>
                <th>Gap</th>
                <th>Nilai Gap</th>
                <th>Rata - Rata</th>
                <th>Total Nilai</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($result['atlet'] as $key => $item)
                <tr>
                    @php
                    if (!empty($result['nilai'][$key])) {
                        $count = (count($result['nilai'][$key]));
                    }else {
                        $count =1 ;
                    }
                    if (!empty($result['total'][$key])) {
                        $countncf = (count(App\Models\Kriteria::where('jenis',1)->get()));
                        $countnsf = (count(App\Models\Kriteria::where('jenis',2)->get()));
                    }else {
                        $countncf =1 ;
                        $countnsf =1;
                    }
                    @endphp
                    <td rowspan="{{$count}}">{{$key+1}}</td>
                    <td rowspan="{{$count}}">{{$item->name}}</td>
                    <td rowspan="{{$count}}">{{$item->kategori->kategori}}</td>
                    @if (!empty($result['nilai'][$key]))   
                        <td>{{ $result['nilai'][$key][0]->created_at }}</td>
                        <td>{{ $result['nilai'][$key][0]->kriteria->name }}</td>
                        <td>{{ $result['nilai'][$key][0]->subkriteria->name }}</td>
                        <td>{{ $result['nilai'][$key][0]->subkriteria->nilai }}</td>
                        <td>{{ $result['nilai'][$key][0]->kriteria->nilai_standar }}</td>
                        <td>{{ $result['nilai'][$key][0]->subkriteria->nilai - $result['nilai'][$key][0]->kriteria->nilai_standar }}</td> 
                        <td>{{ $result['nilai'][$key][0]->gap }}</td>
                        <td rowspan="{{ $countncf }}">{{ $result['total'][$key][0]->ncf }}</td>
                        <td rowspan="{{$count}}">{{$result['total'][$key][0]->total}}</td>

                    @else
                        <td colspan="9">Nilai Belum di Isi</td>
                    @endif
                    {{-- <td rowspan="{{$count}}">{{$item->kategori->kategori}}</td> --}}

                </tr>
                @if (!empty($result['nilai'][$key]))    
                    @for ($i = 1; $i < $count; $i++)
                    <tr>
                        <td>{{ $result['nilai'][$key][$i]->created_at }}</td>
                        <td>{{ $result['nilai'][$key][$i]->kriteria->name }}</td>
                        <td>{{ $result['nilai'][$key][$i]->subkriteria->name }}</td>
                        <td>{{ $result['nilai'][$key][$i]->subkriteria->nilai }}</td>
                        <td>{{ $result['nilai'][$key][$i]->kriteria->nilai_standar }}</td>
                        <td>{{ $result['nilai'][$key][$i]->subkriteria->nilai - $result['nilai'][$key][$i]->kriteria->nilai_standar }}</td>
                        <td>{{ $result['nilai'][$key][$i]->gap }}</td>
                        @if ($i == $countncf)
                        <td rowspan="{{ $countnsf }}">{{ $result['total'][$key][0]->nsf }}</td> 
                        @endif
                    </tr>
                    @endfor
                @endif
               
            @endforeach
        </tbody>
    </table>
</div>
@endsection