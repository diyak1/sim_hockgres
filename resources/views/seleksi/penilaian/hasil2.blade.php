@extends('layout.template')
@section('title')
    Halaman Hasil Perankingan Atlet
@endsection
@section('title-content')
    Hasil Perangkingan Atlet - Kompetisi {{ $jadkom->name }}
@endsection
@section('content')
<div class="card-body">
    <table class="table table-bordered table-striped" >
        <thead>
            <tr align="center">
                <th width="100px">Rangking</th>
                <th>Nama Atlet</th>
                <th>Nilai Profile Matching</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th width="100px">Rangking</th>
                <th>Nama Atlet</th>
                <th>Nilai Profile Matching</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($totalnilai as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->atlet->name}}</td>
                    <td>{{$item->total}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection