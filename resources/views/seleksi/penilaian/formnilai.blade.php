@extends('layout.template')
@section('title')
    Halaman Form Penilaian Atlet
@endsection
@section('title-content')
    Form Penilaian Atlet - {{ $atlet->name }}
@endsection
@section('content')
<form method="POST" action="/pelatih/form-atlet/{{ $id_jadkom }}/{{ $atlet->id }}" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <table class="table table-bordered table-striped" >
            <thead>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Kriteria</th>
                    <th>Sub-Kriteria</th>
                </tr>
            </thead>
            <tfoot>
                <tr align="center">
                    <th width="100px">No</th>
                    <th>Kriteria</th>
                    <th>Sub-Kriteria</th>
                </tr>
            </tfoot>
            <tbody align="center">
                @foreach ($kriteria as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        @php
                            $sub = App\Models\Subkriteria::where('id_kriteria',$item->id)->get();
                        @endphp
                        <td>
                            <input type="text" name="kriteria[]" value="{{ $item->id }}" style="display: none">
                            <select class="form-control" name="subkriteria[]" id="subkriteria">
                                @foreach ($sub as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }} - Nilai Profil : {{ $value->nilai }}</option>   
                                @endforeach
                            </select>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
<button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection