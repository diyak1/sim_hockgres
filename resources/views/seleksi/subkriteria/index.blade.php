@extends('layout.template')
@section('title')
    Halaman Sub-Kriteria
@endsection
@section('title-content')
    Data Sub-Kriteria
@endsection
@section('content')
<a href="/pelatih/tambah-sub_kriteria" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Sub-Kriteria</a>
<div class="card-body">
    <table id="template" class="table table-bordered table-hover" >
        <thead>
            <tr>
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Nama Sub-Kriteria</th>
                <th class="text-center">Kriteria</th>
                <th class="text-center">Nilai</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Nama Sub-Kriteria</th>
                <th class="text-center">Kriteria</th>
                <th class="text-center">Nilai</th>
                <th class="text-center">Action</th>
            </tr>
        </tfoot>
        <tbody >
            @foreach ($subkriteria as $key => $item)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="text-center">{{$item->name}}</td>
                    <td class="text-center">{{$item->kriteria->name}}</td>
                    <td class="text-center">{{$item->nilai}}</td>
                    <td class="text-center">
                        <a type="button" href="/pelatih/sub_kriteria/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection