@extends('layout.template')
@section('title')
    Halaman Tambah Data Sub-Kriteria
@endsection
@section('title-content')
    Tambah Data Sub-Kriteria
@endsection
@section('content')
<form method="POST" action="/pelatih/sub_kriteria/add" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="subkriteria">Nama Sub-Kriteria</label>
        <select class="form-control" name="name" id="subkriteria">
          <option value="Sangat Baik">Sangat Baik</option>
          <option value="Baik">Baik</option>
          <option value="Cukup">Cukup</option>
          <option value="Kurang">Kurang</option>
          <option value="Sangat Kurang">Sangat Kurang</option>
        </select>
      </div>
    <div class="form-group">
        <label for="kriteria">Kriteria</label>
        <select class="form-control" name="id_kriteria" id="kriteria">
            @foreach ($kriteria as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="nilai">Nilai</label>
        <select class="form-control" name="nilai" id="nilai">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
      </div>
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection