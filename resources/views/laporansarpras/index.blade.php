@extends('layout.template')
@section('title')
    Halaman Laporan Sarpras - {{$user->role}}
@endsection
@section('title-content')
    Data Laporan Sarpras
@endsection
@section('content')
<a href="/{{Auth::user()->role}}/laporansarpras/buatlaporan" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Buat Laporan</a>
<a href="/download/sop" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm mb-3">Download SOP</a>

<div class="card-body">
    <table id="example1" class="table table-bordered table-hover" >
        <thead>
            <tr align="center">
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Sarana Prasarana</th>
                <th class="text-center">Nama Pelapor</th>
                <th class="text-center">Tingkat Kerusakan</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Bukti</th>
            </tr>
        </thead>
        <tfoot>
            <tr align="center">
                <th class="text-center" width="100px">No</th>
                <th class="text-center">Sarana Prasarana</th>
                <th class="text-center">Nama Pelapor</th>
                <th class="text-center">Tingkat Kerusakan</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Bukti</th>
            </tr>
        </tfoot>
        <tbody align="center">
            @foreach ($laporansarpras as $key => $item)
                    <tr>
                        <td class="text-center">{{$key+1}}</td>
                        <td class="text-center">{{$item->sarpras->name}}</td>
                        <td class="text-center">{{$item->pelapor->name}}</td>
                        <td class="text-center">{{$item->tingkat_kerusakan}}</td>
                        <td class="text-center">{{$item->keterangan}}</td>
                        <td class="text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{ $item->id }}">
                                Lihat Bukti
                              </button>
                              
                              <!-- Modal -->
                              <div class="modal fade" id="exampleModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Gambar {{ $item->name }}</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <img src="{{url('gambar/' . $item->gambar)}}" style="width:200px" />
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                        </td>
                    </tr>
                @endforeach
        </tbody>
    </table>
</div>
@endsection