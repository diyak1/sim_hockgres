@extends('layout.template')
@section('title')
    Halaman Form Laporan Sarpras - {{$user->role}}
@endsection
@section('title-content')
    Form Laporan Sarpras
@endsection
@section('content')
<form method="POST" action="/{{ Auth::user()->role }}/laporansarpras/add" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="kelas">Sarana Prasarana</label>
        <select class="form-control" name="id_sarpras" id="kelas">
            @foreach ($sarpras as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="kelas">Tingat Kerusakan</label>
        <select class="form-control" name="tingkat_kerusakan" id="kelas">
            <option value="ringan">Ringan</option>
            <option value="sedang">Sedang</option>
            <option value="berat">Berat</option>
        </select>
    </div>
    <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <input type="textarea" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan laporan">
    </div>
    <div class="mb-3">
        <label for="gambar" class="form-label">Masukkan Bukti</label>
        <input class="form-control" type="file" id="gambar" name="gambar">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection