@extends('layout.template')
@section('title')
    Halaman Akun Atlet
@endsection
@section('title-content')
    Data Atlet
@endsection
@section('content')
<div class="button-action" style="margin-bottom: 20px">
    <a href="/manajer/akunatlet/add-dataatlet" class="btn btn-primary">Tambah Data Atlet</a>
</div>

<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="all-tab" data-toggle="tab" data-target="#all" type="button" role="tab" aria-controls="home" aria-selected="true">All</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="false">Indoor Putra</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Indoor Putri</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="contact-tab" data-toggle="tab" data-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Outdoor Putra</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="outdoor-tab" data-toggle="tab" data-target="#outdoor" type="button" role="tab" aria-controls="outdoor" aria-selected="false">Outdoor Putri</button>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                        </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @foreach ($atlet as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->user->email}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td><a type="button" href="/manajer/akunatlet/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                    $nomer=0;
                @endphp
                    @foreach ($indoorputra as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->user->email}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td><a type="button" href="/manajer/akunatlet/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                        </tr>
                    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                    $nomer=0;
                @endphp
                    @foreach ($indoorputri as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->user->email}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td><a type="button" href="/manajer/akunatlet/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                    $nomer=0;
                @endphp
                    @foreach ($outdoorputra as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->user->email}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td><a type="button" href="/manajer/akunatlet/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="outdoor" role="tabpanel" aria-labelledby="outdoor-tab">
        <div class="card-body">
            <table id="" class="table table-bordered table-striped" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Kategori</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                    $nomer=0;
                @endphp
                    @foreach ($outdoorputri as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->user->email}}</td>
                            <td>{{$item->jenis_kelamin}}</td>
                            <td>{{$item->umur}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->kategori->kategori}}</td>
                            <td><a type="button" href="/manajer/akunatlet/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>

@endsection