@extends('layout.template')
@section('title')
    Halaman Akun Atlet
@endsection
@section('title-content')
    Tambah Data Atlet
@endsection
@section('content')
<form method="POST" action="/manajer/akunatlet/add">
    @csrf
    <div class="form-group">
      <label for="namaatlet">Nama Atlet</label>
      <input type="" class="form-control" name="name" id="namaatlet" placeholder="Masukkan Nama Atlet">
    </div>
    <div class="form-group">
        <label for="emailatlet">Email Atlet</label>
        <input type="" class="form-control" name="email" value="@gmail.com" id="emailatlet" placeholder="Masukkan email atlet">
    </div>
    <div class="form-group">
      <label for="jeniskelamin">Jenis Kelamin</label>
      <select class="form-control" name="jenis_kelamin" id="jeniskelamin">
        <option value="Laki - laki">Laki - laki</option>
        <option value="Perempuan">Perempuan</option>
      </select>
  </div>
  <div class="form-group">
    <label for="umuratlet">Umur</label>
    <input type="" class="form-control" name="umur" id="umuratlet" placeholder="Masukkan umur">
  </div>
  <div class="form-group">
    <label for="alamatatlet">Alamat</label>
    <input type="" class="form-control" name="alamat" id="alamatatlet" placeholder="Masukkan alamat">
  </div>
    <div class="form-group">
        <label for="kategori">Kategori Cabor</label>
        <select class="form-control" name="id_kategori" id="kategori">
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->kategori}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group" style="display: none">
        <label for="passwordatlet">Password</label>
        <input type="" class="form-control" name="password" value="12345678" id="passwordatlet" placeholder="Masukkan password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection