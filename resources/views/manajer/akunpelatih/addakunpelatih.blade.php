@extends('layout.template')
@section('title')
    Halaman Akun Pelatih
@endsection
@section('title-content')
    Tambah Data Pelatih
@endsection
@section('content')
<form method="POST" action="/manajer/akunpelatih/add">
    @csrf
    <div class="form-group">
      <label for="namapelatih">Nama Pelatih</label>
      <input type="" class="form-control" name="name" id="namapelatih" placeholder="Masukkan Nama Pelatih">
    </div>
    <div class="form-group">
        <label for="emailpelatih">Email Pelatih</label>
        <input type="" class="form-control" name="email" value="@gmail.com" id="emailpelatih" placeholder="Masukkan email Pelatih">
    </div>
  <div class="form-group">
    <label for="umurpelatih">Umur</label>
    <input type="" class="form-control" name="umur" id="umurpelatih" placeholder="Masukkan Umur">
  </div>
  <div class="form-group">
    <label for="alamatpelatih">Alamat</label>
    <input type="" class="form-control" name="alamat" id="alamatpelatih" placeholder="Masukkan Alamat">
  </div>
    <div class="form-group" style="display: none">
        <label for="passwordpelatih">Password</label>
        <input type="" class="form-control" name="password" value="12345678" id="passwordpelatih" placeholder="Masukkan Password">
    </div>
    <div class="form-group">
        <label for="role">Role</label>
        <select class="form-control" name="role" id="role">
          <option value="pelatih">Pelatih</option>
          <option value="keuangan">Staff Keuangan</option>
          <option value="sarpras">Staff Sarpras</option>
        </select>
    </div>
    <div class="form-group" style="display: none">
        <label for="passwordpelatih">Password</label>
        <input type="" class="form-control" name="password" value="12345678" id="passwordpelatih" placeholder="Masukkan password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection