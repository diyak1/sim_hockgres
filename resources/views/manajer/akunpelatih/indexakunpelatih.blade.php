@extends('layout.template')
@section('title')
    Halaman Akun Pelatih & Staff
@endsection
@section('title-content')
    Data Pelatih & Staff
@endsection
@section('content')
<a href="/manajer/akunpelatih/add-datapelatih" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-3">Tambah Data Pelatih</a>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="all-tab" data-toggle="tab" data-target="#all" type="button" role="tab" aria-controls="home" aria-selected="true">All</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="false">Pelatih</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Staf Sarpras</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="contact-tab" data-toggle="tab" data-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Staf Keuangan</button>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
        <div class="card-body">
            <table id="template" class="table table-bordered table-hover" >
                <thead>
                    <tr>
                        <th class="text-center" width="100px">No</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Umur</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center">Role</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-center" width="100px">No</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Umur</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center">Role</th>
                        <th class="text-center">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($pelatih as $key => $item)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">{{$item->name}}</td>
                            <td class="text-center">{{$item->user->email}}</td>
                            <td class="text-center" >{{$item->umur}}</td>
                            <td class="text-center">{{$item->alamat}}</td>
                            <td class="text-center">{{$item->user->role}}</td>
                            <td class="text-center"><a type="button" href="/manajer/akunpelatih/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                            </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="card-body">
            <table id="tabel1" class="table table-bordered table-hover" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>                        
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer = 0;
                    @endphp
                    @foreach ($pelatih as $key => $item)
                            @foreach ($datapelatih as $value)
                                @if ($value->id == $item->id_user)
                                @php
                                    $nomer = $nomer + 1;
                                @endphp
                                    <tr>
                                        <td>{{$nomer}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->user->email}}</td>
                                        <td>{{$item->umur}}</td>
                                        <td>{{$item->alamat}}</td>
                                        <td>{{$item->user->role}}</td>
                                        <td><a type="button" href="/manajer/akunpelatih/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach        
                </tbody>
            </table>
        </div>  
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card-body">
            <table id="tabel1" class="table table-bordered table-hover" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>                        
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                @php
                    $nomer=0;
                @endphp
                    @foreach ($pelatih as $key => $item)
                            @foreach ($datasarpras as $value)
                                @if ($value->id == $item->id_user)
                                @php
                                    $nomer = $nomer + 1;
                                @endphp
                                    <tr>
                                        <td>{{$nomer}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->user->email}}</td>
                                        <td>{{$item->umur}}</td>
                                        <td>{{$item->alamat}}</td>
                                        <td>{{$item->user->role}}</td>
                                        <td><a type="button" href="/manajer/akunpelatih/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach        
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        <div class="card-body">
            <table id="tabel1" class="table table-bordered table-hover" >
                <thead>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>                        
                </thead>
                <tfoot>
                    <tr align="center">
                        <th width="100px">No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Umur</th>
                        <th>Alamat</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody align="center">
                    @php
                        $nomer = 0;
                    @endphp
                    @foreach ($pelatih as $key => $item)
                            @foreach ($datakeuangan as $value)
                                @if ($value->id == $item->id_user)
                                @php
                                    $nomer = $nomer + 1;
                                @endphp
                                    <tr>
                                        <td>{{$nomer}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->user->email}}</td>
                                        <td>{{$item->umur}}</td>
                                        <td>{{$item->alamat}}</td>
                                        <td>{{$item->user->role}}</td>
                                        <td><a type="button" href="/manajer/akunpelatih/delete{{$item->id}}" class="btn btn-danger m-1">Hapus</a></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach        
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection