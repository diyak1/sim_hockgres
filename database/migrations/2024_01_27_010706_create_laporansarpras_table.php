<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('laporansarpras', function (Blueprint $table) {
            $table->id();
            $table->string('id_sarpras');
            $table->string('id_pelapor');
            $table->enum('tingkat_kerusakan', ['ringan', 'sedang', 'berat']);
            $table->string('keterangan');
            $table->string('gambar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('laporansarpras');
    }
};
