<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kriteria extends Model
{
    protected $table = "kriteria";

    protected $fillable = ["name", "jenis", "nilai_standar"];
    use HasFactory;

    public function subkriteria()
    {
        return $this->hasMany(Subkriteria::class);
    }
}
