<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table = "absensi";

    protected $fillable = ["id_atlet", "id_jadlat", "bulan", "status", "keterangan"];

    use HasFactory;

    public function atlet()
    {
        return $this->belongsTo(Atlet::class, 'id_atlet');
    }

    public function jadlat()
    {
        return $this->belongsTo(Jadlat::class, 'id_jadlat');
    }
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'id_kategori');
    }
}
