<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Jadlat;

class Pelatih extends Model
{
    protected $table = "pelatih";

    protected $fillable = ["id_user", "name", "umur", "alamat"];
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function jadlat()
    {
        return $this->hasMany(Jadlat::class);
    }
}
