<?php

namespace App\Models;

use App\Models\User;
use App\Models\Kategori;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Atlet extends Model
{
    protected $table = "atlet";

    protected $fillable = ["id_user", "name", "jenis_kelamin", "umur", "alamat", "id_kategori"];
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'id_kategori');
    }

    public function gaji()
    {
        return $this->hasMany(Gaji::class);
    }

    public function absensi()
    {
        return $this->hasMany(Absensi::class);
    }
}
