<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pelatih;
use App\Models\Kategori;

class Jadlat extends Model
{
    protected $table = "jadlat";

    protected $fillable = ["name", "id_kategori", "start", "end", "deskripsi", "status"];
    use HasFactory;

    public function pelatih()
    {
        return $this->belongsTo(Pelatih::class, 'id_pelatih');
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'id_kategori');
    }

    public function absensi()
    {
        return $this->hasOne(Absensi::class);
    }
}
