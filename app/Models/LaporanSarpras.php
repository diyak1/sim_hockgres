<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sarpras;
use App\Models\User;
use App\Models\PengajuanDetail;

class LaporanSarpras extends Model
{
    protected $table = "laporansarpras";

    protected $fillable = ["id_sarpras", "id_pelapor", "tingkat_kerusakan", "keterangan", "gambar", "status"];

    use HasFactory;

    public function sarpras()
    {
        return $this->belongsTo(Sarpras::class, 'id_sarpras');
    }

    public function pelapor()
    {
        return $this->belongsTo(User::class, 'id_pelapor');
    }

    public function pengajuan_detail()
    {
        return $this->hasMany(PengajuanDetail::class);
    }
}
