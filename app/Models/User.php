<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Atlet;
use App\Models\Pelatih;
use App\Models\Jadkom;
use App\Models\LaporanSarpras;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function atlet()
    {
        return $this->hasOne(Atlet::class);
    }

    public function pelatih()
    {
        return $this->hasOne(Pelatih::class);
    }

    public function jadkom()
    {
        return $this->hasMany(Jadkom::class);
    }

    public function LaporanSarpras()
    {
        return $this->hasMany(LaporanSarpras::class);
    }

    public function pengajuan_sarpras()
    {
        return $this->hasMany(PengajuanSarpras::class);
    }
    public function penerima_pengajuan_sarpras()
    {
        return $this->hasMany(PengajuanSarpras::class);
    }
}
