<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $table = "penilaian";

    protected $fillable = ["id_atlet", "id_jadkom", 'id_kriteria', 'id_subkriteria', 'gap'];
    use HasFactory;

    public function atlet()
    {
        return $this->belongsTo(Atlet::class, 'id_atlet');
    }

    public function jadkom()
    {
        return $this->belongsTo(Jadkom::class, 'id_jadkom');
    }

    public function kriteria()
    {
        return $this->belongsTo(Kriteria::class, 'id_kriteria');
    }

    public function subkriteria()
    {
        return $this->belongsTo(Subkriteria::class, 'id_subkriteria');
    }
}
