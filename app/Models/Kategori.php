<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Atlet;
use App\Models\Jadlat;

class Kategori extends Model
{
    protected $table = "kategori";

    protected $fillable = ["kategori"];

    use HasFactory;

    public function atlet()
    {
        return $this->hasMany(Atlet::class);
    }

    public function jadlat()
    {
        return $this->hasMany(Jadlat::class);
    }

    public function jadkom()
    {
        return $this->hasMany(Jadkom::class);
    }

    public function settinggaji()
    {
        return $this->hasMany(SettingGaji::class);
    }
}
