<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PengajuanDetail;
use App\Models\User;
use App\Models\Sarpras;

class PengajuanSarpras extends Model
{
    protected $table = "pengajuansarpras";

    protected $fillable = ["id_sarpras", "keterangan", "nominal", "status", "id_pengaju", "id_penerima"];
    use HasFactory;

    public function detail()
    {
        return $this->hasMany(PengajuanDetail::class);
    }

    public function sarpras()
    {
        return $this->belongsTo(Sarpras::class, 'id_sarpras');
    }

    public function pengaju()
    {
        return $this->belongsTo(User::class, 'id_pengaju');
    }

    public function penerima()
    {
        return $this->belongsTo(User::class, 'id_penerima');
    }
}
