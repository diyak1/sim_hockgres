<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Totalnilai extends Model
{
    protected $table = "totalnilai";

    protected $fillable = ["id_atlet", "id_jadkom", "ncf", "nsf", "total"];

    use HasFactory;

    public function atlet()
    {
        return $this->belongsTo(Atlet::class, 'id_atlet');
    }

    public function jadkom()
    {
        return $this->belongsTo(Jadkom::class, 'id_jadkom');
    }
}
