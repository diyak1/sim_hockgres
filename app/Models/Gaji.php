<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gaji extends Model
{
    protected $table = "gaji";

    protected $fillable = ["id_atlet", "nominal", "bulan", "tanggal_pencairan", "presentase"];
    use HasFactory;

    public function atlet()
    {
        return $this->belongsTo(Atlet::class, 'id_atlet');
    }
}
