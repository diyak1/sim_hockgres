<?php

namespace App\Imports;

use App\Models\Atlet;
use App\Models\Kategori;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class AtletImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $rows)
    {
        // dd($);
        foreach ($rows as $row) {
            dd($row[1]);
            $user = User::create([
                'name' => $row[0],
                'email'    => $row[1],
                'role' => 'atlet',
                'password' => Hash::make($row[5]),
            ]);
            $kategori = Kategori::where('kategori', $row[6])->first();
            $atlet = Atlet::create([
                'name' => $row[0],
                'id_user' => $user->id,
                'jenis_kelamin' => $row[2],
                'umur' => $row[3],
                'alamat' => $row[4],
                'id_kategori' => $kategori->id,
            ]);

            
        }
    }
}
