<?php

namespace App\Imports;

use App\Models\Kriteria;
use Maatwebsite\Excel\Concerns\ToModel;


class KriteriaImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $rows)
    {
        foreach ($rows as $row) {
            // dd($row[1]);
            $kriteria = Kriteria::create([
                'name' => $row[0],
                'jenis'    => $row[1],
                'nilai_standar' => $row[2],
            ]);

        // dd($row[1]);
        // return new Kriteria([
        //     'name'              => $row[0], 
        //     'jenis'             => $row[1],
        //     'nilai_standar'     => $row[2],
        // ]);
        }
    }
}
