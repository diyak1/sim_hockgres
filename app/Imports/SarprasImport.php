<?php

namespace App\Imports;

use App\Models\Sarpras;
use Maatwebsite\Excel\Concerns\ToModel;

class SarprasImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Sarpras([
            "name"      => $row[0],
            "jumlah"    => $row[1],
            "harga"     => $row[2],
            "gambar"    => $row[3],
            "detail"    => $row[4],
        ]);
    }
}
