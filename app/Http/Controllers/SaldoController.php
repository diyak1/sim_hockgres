<?php

namespace App\Http\Controllers;

use App\Models\Saldo;
use Illuminate\Http\Request;

class SaldoController extends Controller
{
    public function indexkeuangan()
    {
        $keuangan = Saldo::get();
        $saldo = 0;
        foreach ($keuangan as $value) {
            if ($value->tipe == 1) {
                $saldo = $saldo + $value->nominal;
            } else {
                $saldo = $saldo - $value->nominal;
            }
        }
        return view('keuangan.saldo.index', compact('keuangan', 'saldo'));
    }

    public function indexaddkeuangan()
    {
        return view('keuangan.saldo.add');
    }

    public function actionadd(Request $request)
    {
        $keuangan = new Saldo();
        $keuangan->tipe = $request->tipe;
        $keuangan->nominal = $request->nominal;
        $keuangan->keterangan = $request->keterangan;
        $keuangan->save();

        return redirect('/keuangan/data-keuangan');
    }

    //Manajer
    public function index()
    {
        $keuangan = Saldo::get();
        $saldo = 0;
        foreach ($keuangan as $value) {
            if ($value->tipe == 1) {
                $saldo = $saldo + $value->nominal;
            } else {
                $saldo = $saldo - $value->nominal;
            }
        }
        return view('manajer.indexdatakeuangan', compact('keuangan', 'saldo'));
    }
}
