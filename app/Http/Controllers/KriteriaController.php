<?php

namespace App\Http\Controllers;

use App\Imports\KriteriaImport;
use App\Models\Kategori;
use App\Models\Kriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class KriteriaController extends Controller
{
    public function indexkriteria()
    {
        $kriteria = Kriteria::get();
        return view('seleksi.kriteria.index', compact('kriteria'));
    }

    public function addkriteria()
    {
        return view('seleksi.kriteria.add');
    }

    public function actionadd(Request $request)
    {
        $kriteria = new Kriteria();
        $kriteria->name = $request->name;
        $kriteria->jenis = $request->jenis;
        $kriteria->nilai_standar = $request->nilai_standar;
        $kriteria->save();

        return redirect('/pelatih/datakriteria');
    }

    public function deletekriteria(Request $request, $id)
    {
        $kriteria = Kriteria::where('id', $id)->delete();

        return redirect('/pelatih/datakriteria');
    }

    public function importkriteria(Request $request)
    {
        // dd($request->all());

        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = $file->hashName();

        //temporary file
        $path = $file->storeAs('public/excel/', $nama_file);

        // import data
        $import = Excel::import(new KriteriaImport(), storage_path('app/public/excel/' . $nama_file));

        //remove from server
        Storage::delete($path);

        if ($import) {
            //redirect
            return redirect('/pelatih/datakriteria')->with(['success' => 'Data Berhasil Diimport!']);
        } else {
            //redirect
            return redirect('/pelatih/datakriteria')->with(['error' => 'Data Gagal Diimport!']);
        }
    }
}
