<?php

namespace App\Http\Controllers;

use App\Models\Atlet;
use App\Models\Gaji;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GajiController extends Controller
{
    public function indexgaji()
    {
        $user = Auth::user();
        $atlet = Atlet::where('id_user', $user->id)->first();
        $gaji = Gaji::where('id_atlet', $atlet->id)->get();
        // dd($gaji);

        return view('atlet.gaji.indexgaji', compact('gaji', 'user', 'atlet'));
    }
}
