<?php

namespace App\Http\Controllers;

use App\Models\Kriteria;
use App\Models\Subkriteria;
use Illuminate\Http\Request;

class SubkriteriaController extends Controller
{
    public function indexsubkriteria()
    {
        $kriteria = Kriteria::get();
        $subkriteria = Subkriteria::get();
        // dd($subkriteria);
        return view('seleksi.subkriteria.index', compact('kriteria', 'subkriteria'));
    }

    public function addsubkriteria()
    {
        $kriteria = Kriteria::get();
        return view('seleksi.subkriteria.add', compact('kriteria'));
    }

    public function actionadd(Request $request)
    {
        $subkriteria = new Subkriteria();
        $subkriteria->id_kriteria = $request->id_kriteria;
        $subkriteria->name = $request->name;
        $subkriteria->nilai = $request->nilai;
        $subkriteria->save();

        return redirect('/pelatih/data-sub_kriteria');
    }

    public function deletesubkriteria(Request $request, $id)
    {
        $subkriteria = Subkriteria::where('id', $id)->delete();

        return redirect('/pelatih/data-sub_kriteria');
    }
}
