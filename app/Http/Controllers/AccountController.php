<?php

namespace App\Http\Controllers;

use App\Imports\AtletImport;
use App\Models\User;
use App\Models\Atlet;
use App\Models\Kategori;
use App\Models\Pelatih;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class AccountController extends Controller
{
    //Atlet

    public function indexakunatlet()
    {
        $kategori = Kategori::get();
        $atlet = Atlet::get();
        $indoorputra = Atlet::where('id_kategori', 1)->get();
        $indoorputri = Atlet::where('id_kategori', 2)->get();
        $outdoorputra = Atlet::where('id_kategori', 3)->get();
        $outdoorputri = Atlet::where('id_kategori', 4)->get();
        return view('manajer.akunatlet.indexakunatlet', compact('kategori', 'atlet', 'indoorputra', 'indoorputri', 'outdoorputra', 'outdoorputri'));
    }

    public function indexaddakunatlet()
    {
        $kategori = Kategori::get();
        return view('manajer.akunatlet.addakunatlet', compact('kategori'));
    }

    public function addakunatlet(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 'atlet';
        $user->password = Hash::make($request->password);
        $user->save();

        $atlet = new Atlet();
        $atlet->name = $request->name;
        $atlet->id_user = $user->id;
        $atlet->jenis_kelamin = $request->jenis_kelamin;
        $atlet->umur = $request->umur;
        $atlet->alamat = $request->alamat;
        $atlet->id_kategori = $request->id_kategori;
        $atlet->save();

        return redirect('/manajer/akunatlet/dataatlet');
    }

    public function importatlet(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = $file->hashName();

        //temporary file
        $path = $file->storeAs('public/excel/', $nama_file);

        // import data
        $import = Excel::import(new AtletImport(), storage_path('app/public/excel/' . $nama_file));

        //remove from server
        Storage::delete($path);

        if ($import) {
            //redirect
            return redirect('/manajer/akunatlet/dataatlet')->with(['success' => 'Data Berhasil Diimport!']);
        } else {
            //redirect
            return redirect('/manajer/akunatlet/dataatlet')->with(['error' => 'Data Gagal Diimport!']);
        }
    }

    //Pelatih

    public function indexakunpelatih()
    {
        $pelatih = Pelatih::get();
        $datapelatih = User::where('role', 'pelatih')->get();
        $datasarpras = User::where('role', 'sarpras')->get();
        $datakeuangan = User::where('role', 'keuangan')->get();
        return view('manajer.akunpelatih.indexakunpelatih', compact('pelatih', 'datapelatih', 'datasarpras', 'datakeuangan'));
    }

    public function indexaddakunpelatih()
    {
        return view('manajer.akunpelatih.addakunpelatih');
    }

    public function addakunpelatih(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = Hash::make($request->password);
        $user->save();

        $atlet = new Pelatih();
        $atlet->name = $request->name;
        $atlet->id_user = $user->id;
        $atlet->umur = $request->umur;
        $atlet->alamat = $request->alamat;
        $atlet->save();

        return redirect('/manajer/akunpelatih/datapelatih');
    }

    //Pelatih Data Atlet

    public function dataatlet()
    {
        $total = Atlet::count();
        $kategori = Kategori::get();
        $atlet = Atlet::simplePaginate(10);
        $indoorputra = Atlet::where('id_kategori', 1)->get();
        $indoorputri = Atlet::where('id_kategori', 2)->get();
        $outdoorputra = Atlet::where('id_kategori', 3)->get();
        $outdoorputri = Atlet::where('id_kategori', 4)->get();
        return view('pelatih.dataatlet', compact('kategori', 'atlet', 'indoorputra', 'indoorputri', 'outdoorputra', 'outdoorputri', 'total'));
    }
}
