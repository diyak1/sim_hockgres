<?php

namespace App\Http\Controllers;

use App\Imports\SarprasImport;
use App\Models\Sarpras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class SarprasController extends Controller
{
    public function indexdatasarpras()
    {
        $sarpras = Sarpras::get();
        return view('sarpras.datasarpras.index', compact('sarpras'));
    }

    public function indexaddsarpras()
    {
        return view('sarpras.datasarpras.indexadd');
    }

    public function addsarpras(Request $request)
    {
        $sarpras = new Sarpras();

        $image = $request->file('gambar');
        $imagename = time() . '.' . $image->getClientOriginalExtension();
        $request->file('gambar')->move('sarpras/', $imagename);

        $sarpras->name = $request->name;
        $sarpras->jumlah = $request->jumlah;
        $sarpras->harga = $request->harga;
        $sarpras->gambar = $imagename;
        $sarpras->detail = $request->detail;
        $sarpras->save();
        return redirect('/sarpras/data-sarpras');
    }

    public function deletesarpras(Request $request, $id)
    {
        $sarpras = Sarpras::where('id', $id)->delete();

        return redirect('/sarpras/data-sarpras');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = $file->hashName();

        //temporary file
        $path = $file->storeAs('public/excel/', $nama_file);

        // import data
        $import = Excel::import(new SarprasImport(), storage_path('app/public/excel/' . $nama_file));

        //remove from server
        Storage::delete($path);

        if ($import) {
            //redirect
            return redirect('/sarpras/data-sarpras')->with(['success' => 'Data Berhasil Diimport!']);
        } else {
            //redirect
            return redirect('/sarpras/data-sarpras')->with(['error' => 'Data Gagal Diimport!']);
        }
    }
}
