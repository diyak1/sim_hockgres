<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\Atlet;
use App\Models\Gaji;
use App\Models\Kategori;
use App\Models\SettingGaji;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SettingGajiController extends Controller
{
    public function indexsettinggaji()
    {
        $setGaji = SettingGaji::get();
        $kategori = Kategori::get();
        // dd("$kategori");
        return view('keuangan.setgaji.index', compact('setGaji', 'kategori'));
    }

    public function indexeditsetgaji($id)
    {
        $setGaji = SettingGaji::where('id', $id)->first();

        return view('keuangan.setgaji.edit', compact('setGaji'));
    }

    public function actionedit(Request $request, $id)
    {
        $setGaji = SettingGaji::where('id', $id)->first();
        $setGaji->nominal = $request->nominal;
        $setGaji->save();

        return redirect('/keuangan/setting_gaji');
    }

    public function addkategori()
    {
        $kategori = Kategori::get();

        return view('keuangan.setgaji.edit', compact('kategori'));
    }

    public function actionadd(Request $request)
    {
        $setGaji = new SettingGaji();
        $setGaji->id_kategori = $request->id_kategori;
        $setGaji->nominal =0;
        $setGaji->save();
        return redirect('/keuangan/setting_gaji');
    }

    //Generate Gaji

    public function indexgenerate()
    {
        $generate = Kategori::get();

        return view('keuangan.generategaji.index', compact('generate'));
    }

    public function indexaddgenerate($id)
    {
        $kategori = Kategori::where('id', $id)->first();
        return view('keuangan.generategaji.form', compact('kategori'));
    }

    public function actionaddgenerate(Request $request)
    {
        $kategori = Kategori::where('id', $request->id_kategori)->first();
        $atlets = Atlet::where('id_kategori', $request->id_kategori)->get();
        $setGaji = SettingGaji::where('id_kategori', $request->id_kategori)->first();

        foreach ($atlets as $atlet) {
            $absensi = Absensi::where('id_atlet', $atlet->id)->where('bulan', date('F Y', strtotime($request->bulan)))->count();
            $absensihadir = Absensi::where('id_atlet', $atlet->id)->where('bulan', date('F Y', strtotime($request->bulan)))->where('status', 1)->count();
            $gaji = new Gaji;
            $gaji->id_atlet = $atlet->id;
            $gaji->nominal = ($absensihadir / $absensi) * $setGaji->nominal;
            $gaji->bulan = date('F Y', strtotime($request->bulan));
            $gaji->presentase = ($absensihadir / $absensi) * 100;
            $gaji->save();
        }

        return redirect('/keuangan/generategaji');
    }

    //Pencairan

    public function indexpencairan()
    {
        $pencairan = Gaji::get();

        return view('keuangan.pencairan.index', compact('pencairan'));
    }

    public function actionpencairan($id)
    {
        $gaji = Gaji::where('id', $id)->first();
        $gaji->tanggal_pencairan = Carbon::now();
        $gaji->save();

        return redirect('/keuangan/pencairan');
    }
}
