<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function indexkategori()
    {
        $kategori = Kategori::get();
        return view('kategori.indexkategori', compact('kategori'));
    }

    public function indexaddkategori()
    {
        return view('kategori.addkategori');
    }

    public function addkategori(Request $request)
    {
        $kategori = new Kategori();
        $kategori->kategori = $request->kategori;
        $kategori->gender = $request->gender;
        $kategori->save();
        return redirect('/kategori/datakategori');
    }

    public function deletekategori(Request $request, $id)
    {
        $kategori = Kategori::where('id', $id)->delete();

        return redirect('/kategori/datakategori');
    }
}
