<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboardatlet()
    {
        return view('atlet.dashboard');
    }

    public function dashboardmanajer()
    {
        return view('manajer.dashboard');
    }

    public function dashboardpelatih()
    {
        return view('pelatih.dashboard');
    }
    public function dashboardsarpras()
    {
        return view('sarpras.dashboard');
    }
    public function dashboardkeuangan()
    {
        return view('keuangan.dashboard');
    }
}
