<?php

namespace App\Http\Controllers;

use App\Models\LaporanSarpras;
use App\Models\PengajuanDetail;
use App\Models\PengajuanSarpras;
use App\Models\Saldo;
use App\Models\Sarpras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PengajuanSarprasController extends Controller
{
    public function indexpengajuan()
    {
        $pengajuan = PengajuanSarpras::get();
        $sarpras = Sarpras::get();
        return view('sarpras.pengajuan.index', compact('pengajuan', 'sarpras'));
    }

    public function indexcreatepengajuan(Request $request)
    {
        $laporan = LaporanSarpras::where('id_sarpras', $request->id_sarpras)->where('status', 0)->get();
        $idsarpras = $request->id_sarpras;
        return view('sarpras.pengajuan.form', compact('laporan', 'idsarpras'));
    }

    public function addpengajuan(Request $request)
    {
        // dd($request->input());
        $auth = Auth::user();
        $pengajuan = new PengajuanSarpras;
        $pengajuan->id_sarpras = $request->sarpras;
        $pengajuan->keterangan = $request->keterangan;
        $pengajuan->nominal = $request->nominal;
        $pengajuan->status = 0;
        $pengajuan->id_pengaju = $auth->id;
        $pengajuan->save();

        foreach ($request->idlaporan as $id) {
            $detail = new PengajuanDetail();
            $detail->id_pengajuan = $pengajuan->id;
            $detail->id_laporan = $id;
            $detail->save();

            $laporan = LaporanSarpras::where('id', $id)->first();
            $laporan->status = 1;
            $laporan->save();
        }
        return redirect('/sarpras/pengajuan');
    }


    //Keuangan 

    public function indexapprove()
    {
        $pengajuan = PengajuanSarpras::get();

        return view('keuangan.aprrovepengajuan.index', compact('pengajuan'));
    }

    public function accpengajuankeuangan($id)
    {
        $auth = Auth::user();
        $pengajuan = PengajuanSarpras::where('id', $id)->first();
        $pengajuan->status = 1;
        $pengajuan->id_penerima = $auth->id;
        $pengajuan->save();

        $saldo = new Saldo;
        $saldo->tipe = 0;
        $saldo->nominal = $pengajuan->nominal;
        $saldo->keterangan = $pengajuan->keterangan;
        $saldo->save();

        return redirect('/keuangan/pengajuan-sarpras');
    }

    public function rejectpengajuankeuangan($id)
    {
        $auth = Auth::user();
        $pengajuan = PengajuanSarpras::where('id', $id)->first();
        $pengajuan->status = 2;
        $pengajuan->save();

        return redirect('/keuangan/pengajuan-sarpras');
    }
}
