<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        $request->session()->regenerate();
        if (Auth::user() && Auth::user()->role == 'manajer') {
            return redirect()->route('dashboard-manajer');
        } elseif (Auth::user() && Auth::user()->role == 'atlet') {
            return redirect()->route('dashboard-atlet');
        } elseif (Auth::user() && Auth::user()->role == 'pelatih') {
            return redirect()->route('dashboard-pelatih');
        } elseif (Auth::user() && Auth::user()->role == 'sarpras') {
            return redirect()->route('dashboard-sarpras');
        } elseif (Auth::user() && Auth::user()->role == 'keuangan') {
            return redirect()->route('dashboard-keuangan');
        } else {
            Auth::guard('web')->logout();
            return redirect()->route('login')->with('status', 'You are not authorized to access this page.');
        }
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
