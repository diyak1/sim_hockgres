<?php

namespace App\Http\Controllers;

use App\Models\LaporanSarpras;
use App\Models\Sarpras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LaporanSarprasController extends Controller
{
    public function indexdatalaporansarpras()
    {
        $laporansarpras = LaporanSarpras::get();
        return view('sarpras.laporansarpras.index', compact('laporansarpras'));
    }

    public function indexlaporansarpras()
    {
        $user = Auth::user();
        $laporansarpras = LaporanSarpras::where('id_pelapor', $user->id)->get();
        return view('laporansarpras.index', compact('laporansarpras', 'user'));
    }

    public function formlaporansarpras()
    {
        $user = Auth::user();
        $sarpras = Sarpras::get();
        return view('laporansarpras.indexform', compact('user', 'sarpras'));
    }

    public function addlaporan(Request $request)
    {
        $auth = Auth::user();

        $laporansarpras = new LaporanSarpras;

        $image = $request->file('gambar');
        $imagename = time() . '.' . $image->getClientOriginalExtension();
        $request->file('gambar')->move('gambar/', $imagename);

        $laporansarpras->id_sarpras = $request->id_sarpras;
        $laporansarpras->id_pelapor = $auth->id;
        $laporansarpras->tingkat_kerusakan = $request->tingkat_kerusakan;
        $laporansarpras->keterangan = $request->keterangan;
        $laporansarpras->gambar = $imagename;
        $laporansarpras->status = 0;
        $laporansarpras->save();

        return redirect('/' . $auth->role . '/laporansarpras');
    }

    public function download()
    {
        return response()->download(storage_path('SOP PELAPORAN SARPRAS.pdf'));
    }

}
