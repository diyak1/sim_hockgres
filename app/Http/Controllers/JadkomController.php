<?php

namespace App\Http\Controllers;

use App\Models\Jadkom;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use File;

class JadkomController extends Controller
{
    public function indexjadkom()
    {
        $jadkom = Jadkom::get();
        $kategori = Kategori::get();
        return view('jadwal.jadkom.indexjadkom', compact('jadkom', 'kategori'));
    }

    public function indexaddjadkom()
    {
        $kategori = Kategori::get();
        return view('jadwal.jadkom.addjadkom', compact('kategori'));
    }

    public function addjadkom(Request $request)
    {
        $jadkom = new Jadkom();

        $file = $request->file('file');
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $request->file('file')->move('dokumen/', $filename);

        $jadkom->name = $request->name;
        $jadkom->id_kategori = $request->id_kategori;
        $jadkom->waktu = $request->waktu;
        $jadkom->tempat = $request->tempat;
        $jadkom->file = $filename;
        $jadkom->save();
        return redirect('/manajer/jadwal/jadkom/datajadkom');
    }

    public function deletejadkom(Request $request, $id)
    {
        $jadkom = Jadkom::where('id', $id)->delete();

        return redirect('/manajer/jadwal/jadkom/datajadkom');
    }

    public function download(Request $request, $file)
    {

        return response()->download(public_path('dokumen/' . $file));
    }
}
