<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\Atlet;
use App\Models\Jadlat;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AbsensiController extends Controller
{
    //Generate Absensi
    public function indexgenerate()
    {
        $kategori = Kategori::get();
        $jadlat = Jadlat::get();
        $jadlatindoorputra = Jadlat::where('id_kategori', 1)->get();
        $jadlatindoorputri = Jadlat::where('id_kategori', 2)->get();
        $jadlatoutdoorputra = Jadlat::where('id_kategori', 3)->get();
        $jadlatoutdoorputri = Jadlat::where('id_kategori', 4)->get();
        return view('absensi.generateabsensi', compact('jadlat', 'kategori', 'jadlatindoorputra', 'jadlatindoorputri', 'jadlatoutdoorputra', 'jadlatoutdoorputri'));
    }

    public function actiongenerate($id)
    {
        $jadlat = Jadlat::where('id', $id)->first();
        $bulan = date('F Y', strtotime($jadlat->start));
        $atlets = Atlet::where('id_kategori', $jadlat->id_kategori)->get();
        foreach ($atlets as $atlet) {
            $absensi = new Absensi;
            $absensi->id_atlet = $atlet->id;
            $absensi->id_jadlat = $jadlat->id;
            $absensi->bulan = $bulan;
            $absensi->status = 0;
            $absensi->save();
        }

        $jadlat->status = 1;
        $jadlat->save();

        return redirect('/pelatih/generateabsensi');
    }

    //Absensi
    public function pilih()
    {
        $kategori = Kategori::get();
        $jadlat = Jadlat::get();
        $jadlatindoorputra = Jadlat::where('id_kategori', 1)->get();
        $jadlatindoorputri = Jadlat::where('id_kategori', 2)->get();
        $jadlatoutdoorputra = Jadlat::where('id_kategori', 3)->get();
        $jadlatoutdoorputri = Jadlat::where('id_kategori', 4)->get();
        // dd($absensi);
        return view('absensi.indexabsensi2', compact('jadlat', 'kategori', 'jadlatindoorputra', 'jadlatindoorputri', 'jadlatoutdoorputra', 'jadlatoutdoorputri'));
    }

    public function indexabsensiall($id)
    {
        $jadlat = Jadlat::where('id', $id)->first();
        $atlet = Atlet::where('id_kategori', $jadlat->id_kategori)->get();
        $absensi = Absensi::where('id_jadlat', $jadlat->id)->get();
        // dd($absensi);

        return view('absensi.indexabsensi', compact('jadlat', 'atlet', 'absensi',));
    }

    public function index()
    {
        $kategori = Kategori::get();
        $jadlat = Jadlat::get();
        $jadlatindoorputra = Jadlat::where('id_kategori', 1)->get();
        $jadlatindoorputri = Jadlat::where('id_kategori', 2)->get();
        $jadlatoutdoorputra = Jadlat::where('id_kategori', 3)->get();
        $jadlatoutdoorputri = Jadlat::where('id_kategori', 4)->get();
        return view('absensi.index', compact('jadlat', 'kategori', 'jadlatindoorputra', 'jadlatindoorputri', 'jadlatoutdoorputra', 'jadlatoutdoorputri'));
    }

    public function indexaddabsensi($id)
    {
        $absensi = Absensi::where('id_jadlat', $id)->get();
        $jadlat = Jadlat::where('id', $id)->first();
        // dd($absensi);
        return view('absensi.formabsensi', compact('absensi', 'jadlat'));
    }

    public function addabsensi(Request $request)
    {
        foreach ($request->id as $key => $value) {
            $absensi = Absensi::where('id_atlet', $value)->where('id_jadlat', $request->id_jadlat)->first();
            $absensi->status = $request->absensi[$key];
            $absensi->keterangan = $request->keterangan[$key];
            $absensi->save();
        }
        return redirect('/pelatih/absensi');
    }

    //Absensi siswa
    public function indexabsensi()
    {
        $user = Auth::user();
        $atlet = Atlet::where('id_user', $user->id)->first();
        $absensi = Absensi::where('id_atlet', $atlet->id)->get();
        // dd($absensi);
        return view('atlet.absensi.indexabsensi', compact('absensi', 'atlet'));
    }
}
