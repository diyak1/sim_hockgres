<?php

namespace App\Http\Controllers;

use App\Models\Jadlat;
use App\Models\Kategori;
use Illuminate\Http\Request;

class JadlatController extends Controller
{
    public function indexjadlat()
    {
        $kategori = Kategori::get();
        $jadlat = Jadlat::get();
        $jadlatindoorputra = Jadlat::where('id_kategori', 1)->get();
        $jadlatindoorputri = Jadlat::where('id_kategori', 2)->get();
        $jadlatoutdoorputra = Jadlat::where('id_kategori', 3)->get();
        $jadlatoutdoorputri = Jadlat::where('id_kategori', 4)->get();
        return view('jadwal.jadlat.indexjadlat', compact('kategori', 'jadlat', 'jadlatindoorputra', 'jadlatindoorputri', 'jadlatoutdoorputra', 'jadlatoutdoorputri'));
    }

    public function indexaddjadlat()
    {
        $kategori = Kategori::get();
        return view('jadwal.jadlat.addjadlat', compact('kategori'));
    }

    public function addjadlat(Request $request)
    {
        $jadlat = new Jadlat();
        $jadlat->name = $request->name;
        $jadlat->id_kategori = $request->id_kategori;
        $jadlat->start = $request->start;
        $jadlat->end = $request->end;
        $jadlat->deskripsi = $request->deskripsi;
        $jadlat->save();
        return redirect('/pelatih/jadwal/jadlat/datajadlat');
    }

    public function deletejadlat(Request $request, $id)
    {
        $jadlat = Jadlat::where('id', $id)->delete();

        return redirect('/pelatih/jadwal/jadlat/datajadlat');
    }
}
