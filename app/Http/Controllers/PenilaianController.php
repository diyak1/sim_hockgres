<?php

namespace App\Http\Controllers;

use App\Models\Atlet;
use App\Models\Jadkom;
use App\Models\Kategori;
use App\Models\Kriteria;
use App\Models\Penilaian;
use App\Models\Subkriteria;
use App\Models\Totalnilai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PenilaianController extends Controller
{
    public function indexpenilaian()
    {
        // $atlet = Atlet::get();
        // $jadkom = Jadkom::get();
        // $kriteria = Kriteria::get();
        // $subkriteria = Subkriteria::get();
        // return view('seleksi.penilaian.index', compact('atlet', 'jadkom', 'kriteria', 'subkriteria'));
    }

    public function index1()
    {
        $jadkom = Jadkom::get();
        return view('seleksi.penilaian.index1', compact('jadkom'));
    }

    public function index2($id)
    {
        $jadkom = Jadkom::where('id', $id)->first();
        $atlet = Atlet::where('id_kategori', $jadkom->id_kategori)->get();
        $penilaian = Penilaian::where('id_jadkom', $jadkom->id)->get();
        $result['atlet'] = $atlet;
        foreach ($result['atlet'] as $key => $val) {
            $nilai = Penilaian::where('id_jadkom', $jadkom->id)->where('id_atlet', $val->id)->get();
            foreach ($nilai as $key2 => $item) {
                $result['nilai'][$key][$key2] = $item;
            }
            $total = Totalnilai::where('id_jadkom', $jadkom->id)->where('id_atlet', $val->id)->get();
            foreach ($total as $key2 => $item) {
                $result['total'][$key][$key2] = $item;
            }
        }
        // dd($result);
        return view('seleksi.penilaian.index2', compact('jadkom', 'atlet', 'penilaian', 'result'));
    }

    public function indexadd()
    {
        $jadkom = Jadkom::get();
        return view('seleksi.penilaian.form', compact('jadkom'));
    }

    public function indexformatlet($id)
    {
        $jadkom = Jadkom::where('id', $id)->first();
        $atlet = Atlet::where('id_kategori', $jadkom->id_kategori)->get();
        return view('seleksi.penilaian.form2', compact('atlet', 'jadkom'));
    }

    public function indexformnilai($id_jadkom, $id_atlet)
    {
        $atlet = Atlet::where('id', $id_atlet)->first();
        $kriteria = Kriteria::get();
        // $jadkom = Jadkom::where('id', $id)->first();
        // $atlet = Atlet::where('id_kategori', $jadkom->id_kategori)->get();
        return view('seleksi.penilaian.formnilai', compact('atlet', 'kriteria', 'id_jadkom'));
    }

    public function actionadd(Request $request, $id_jadkom, $id_atlet)
    {
        // dd($request->input());
        $datancf = Kriteria::where('jenis', 1)->count();
        $datansf = Kriteria::where('jenis', 2)->count();
        $gap = [];
        $bobot_nilai = [];
        $totalncf = 0;
        $totalnsf = 0;
        $ncf = 0;
        $nsf = 0;
        foreach ($request->kriteria as $key => $value) {
            // dd($key);
            $penilaian = new Penilaian;
            $penilaian->id_atlet = $id_atlet;
            $penilaian->id_jadkom = $id_jadkom;
            $penilaian->id_kriteria = $value;
            $penilaian->id_subkriteria = $request->subkriteria[$key];
            // $penilaian->save();

            $kriteria = Kriteria::where('id', $value)->first();
            $subkriteria = Subkriteria::where('id', $request->subkriteria[$key])->first();
            $gap[$key] = $subkriteria->nilai - $kriteria->nilai_standar;

            if ($gap[$key] == 0) {
                $bobot_nilai[$key] = 5;
            } elseif ($gap[$key] == 1) {
                $bobot_nilai[$key] = 4.5;
            } elseif ($gap[$key] == -1) {
                $bobot_nilai[$key] = 4;
            } elseif ($gap[$key] == 2) {
                $bobot_nilai[$key] = 3.5;
            } elseif ($gap[$key] == -2) {
                $bobot_nilai[$key] = 3;
            } elseif ($gap[$key] == 3) {
                $bobot_nilai[$key] = 2.5;
            } elseif ($gap[$key] == -3) {
                $bobot_nilai[$key] = 2;
            } elseif ($gap[$key] == 4) {
                $bobot_nilai[$key] = 1.5;
            } elseif ($gap[$key] == -4) {
                $bobot_nilai[$key] = 1;
            }

            $penilaian->gap = $bobot_nilai[$key];
            $penilaian->save();

            if ($kriteria->jenis == 1) {
                $totalncf = $totalncf + $bobot_nilai[$key];
            } elseif ($kriteria->jenis == 2) {
                $totalnsf = $totalnsf + $bobot_nilai[$key];
            }
        }
        // dd($bobot_nilai);
        $totalnilai = new Totalnilai;
        $totalnilai->id_atlet = $id_atlet;
        $totalnilai->id_jadkom = $id_jadkom;
        $totalnilai->ncf = $totalncf / $datancf;
        $totalnilai->nsf = $totalnsf / $datansf;
        $totalnilai->total = (($totalncf / $datancf) * 70 / 100) + (($totalnsf / $datansf) * 30 / 100);
        $totalnilai->save();
        return redirect('/pelatih/form-atlet/' . $id_jadkom);
    }

    //Hasil Seleksi (Pelatih)

    public function indexhasil()
    {
        $jadkom = Jadkom::get();
        return view('seleksi.penilaian.hasil1', compact('jadkom'));
    }

    public function hasilseleksi($id)
    {
        $jadkom = Jadkom::where('id', $id)->first();
        $atlet = Atlet::where('id_kategori', $jadkom->id_kategori)->get();
        $totalnilai = Totalnilai::where('id_jadkom', $jadkom->id)->orderBy('total', 'desc')->get();
        // dd($totalnilai);

        return view('seleksi.penilaian.hasil2', compact('jadkom', 'atlet', 'totalnilai'));
    }

    //Hasil Penilaian (Altet)
    public function indexhasilatlet()
    {
        $user = Auth::user();
        $atlet = Atlet::where('id_user', $user->id)->first();
        $kategori = Kategori::where('id', $atlet->id_kategori)->first();
        $jadkom = Jadkom::where('id_kategori', $kategori->id)->get();
        return view('atlet.seleksi.indexhasil', compact('jadkom'));
    }

    public function indexpenilaianatlet($id)
    {
        $user = Auth::user();
        $jadkom = Jadkom::where('id', $id)->first();
        $atlet = Atlet::where('id_user', $user->id)->first();
        $penilaian = Penilaian::where('id_jadkom', $jadkom->id)->get();
        $result['atlet'] = $atlet;
        $nilai = Penilaian::where('id_jadkom', $jadkom->id)->where('id_atlet', $atlet->id)->get();
        foreach ($nilai as $key2 => $item) {
            $result['nilai'][$key2] = $item;
        }
        $total = Totalnilai::where('id_jadkom', $jadkom->id)->where('id_atlet', $atlet->id)->get();
        foreach ($total as $key2 => $item) {
            $result['total'][$key2] = $item;
        }
        // dd($result);
        return view('atlet.seleksi.indexhasil2', compact('jadkom', 'atlet', 'penilaian', 'result'));
    }

    //Hasil Ranking (Atlet)

    public function indexpilihjadkom()
    {
        $user = Auth::user();
        $atlet = Atlet::where('id_user', $user->id)->first();
        $kategori = Kategori::where('id', $atlet->id_kategori)->first();
        $jadkom = Jadkom::where('id_kategori', $kategori->id)->get();

        return view('atlet.ranking.indexranking', compact('jadkom'));
    }

    public function indexranking($id)
    {
        $user = Auth::user();
        $jadkom = Jadkom::where('id', $id)->first();
        $atlet = Atlet::where('id_user', $user->id)->first();
        $totalnilai = Totalnilai::where('id_jadkom', $jadkom->id)->orderBy('total', 'desc')->get();

        // dd($totalnilai);

        return view('atlet.ranking.indexranking2', compact('jadkom', 'totalnilai'));
    }
}
