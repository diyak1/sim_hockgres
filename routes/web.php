<?php

use App\Http\Controllers\ProfileController;
use App\Models\Kategori;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware(['auth']);

// Route::get('/dashboard', function () {
//     return view('atlet.dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard-atlet');
Route::get('/download/sop', [App\Http\Controllers\LaporanSarprasController::class, 'download'])->name('dashboard-manajer');

// Route Manajer
Route::group(['middleware' => 'role:manajer'], function () {

    //Dashboard
    Route::get('/manajer/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardmanajer'])->name('dashboard-manajer');

    //Akun Atlet
    Route::get('/manajer/akunatlet/dataatlet', [App\Http\Controllers\AccountController::class, 'indexakunatlet'])->name('indexakunatlet-manajer');
    Route::get('/manajer/akunatlet/add-dataatlet', [\App\Http\Controllers\AccountController::class, 'indexaddakunatlet'])->name('add-dataatlet-manajer');
    Route::post('/manajer/akunatlet/add', [\App\Http\Controllers\AccountController::class, 'addakunatlet'])->name('addakunatlet-manajer');
    Route::post('/manajer/data-atlet/import-excel', [\App\Http\Controllers\AccountController::class, 'importatlet'])->name('importdatatlet-manajer');

    //Akun Pelatih
    Route::get('/manajer/akunpelatih/datapelatih', [App\Http\Controllers\AccountController::class, 'indexakunpelatih'])->name('indexakunpelatih-manajer');
    Route::get('/manajer/akunpelatih/add-datapelatih', [\App\Http\Controllers\AccountController::class, 'indexaddakunpelatih'])->name('add-datapelatih-manajer');
    Route::post('/manajer/akunpelatih/add', [\App\Http\Controllers\AccountController::class, 'addakunpelatih'])->name('addakunpelatih-manajer');

    //Kategori
    Route::get('/kategori/datakategori', [\App\Http\Controllers\KategoriController::class, 'indexkategori'])->name('indexkategori-manajer');
    Route::get('/kategori/addkategori', [\App\Http\Controllers\KategoriController::class, 'indexaddkategori'])->name('addkategori-manajer');
    Route::post('/kategori/add', [App\Http\Controllers\KategoriController::class, 'addkategori'])->name('add-manajer');
    Route::get('/kategori/delete{id}', [\App\Http\Controllers\KategoriController::class, 'deletekategori'])->name('deletekategori-manajer');

    //Jadkom
    Route::get('/manajer/jadwal/jadkom/datajadkom', [\App\Http\Controllers\JadkomController::class, 'indexjadkom'])->name('indexjadkom-manajer');
    Route::get('/manajer/jadwal/jadkom/add-datajadkom', [\App\Http\Controllers\JadkomController::class, 'indexaddjadkom'])->name('add-datajadkom-manajer');
    Route::post('/manajer/jadwal/jadkom/add-jadkom', [App\Http\Controllers\JadkomController::class, 'addjadkom'])->name('addjadkom-manajer');
    Route::get('/manajer/jadwal/jadkom/delete{id}', [\App\Http\Controllers\JadkomController::class, 'deletejadkom'])->name('deletejadkom-manajer');
    Route::get('/manajer/download/{file}', [\App\Http\Controllers\JadkomController::class, 'download'])->name('downloadfile-manajer');

    //Data Keuangan
    Route::get('/manajer/data-keuangan', [\App\Http\Controllers\SaldoController::class, 'index'])->name('index-manajer');
});

// Pelatih
Route::group(['middleware' => 'role:pelatih'], function () {

    //Dashboard
    Route::get('/pelatih/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardpelatih'])->name('dashboard-pelatih');

    //Data Atlet
    Route::get('/pelatih/data-atlet', [App\Http\Controllers\AccountController::class, 'dataatlet'])->name('dataatlet-pelatih');


    //Jadlat
    Route::get('/pelatih/jadwal/jadlat/datajadlat', [\App\Http\Controllers\JadlatController::class, 'indexjadlat'])->name('indexjadlat-pelatih');
    Route::get('/pelatih/jadwal/jadlat/add-datajadlat', [\App\Http\Controllers\JadlatController::class, 'indexaddjadlat'])->name('add-datajadlat-pelatih');
    Route::post('/pelatih/jadwal/jadlat/add-jadlat', [App\Http\Controllers\JadlatController::class, 'addjadlat'])->name('addjadlat-pelatih');
    Route::get('/pelatih/jadwal/jadlat/delete{id}', [\App\Http\Controllers\JadlatController::class, 'deletejadlat'])->name('deletejadlat-pelatih');

    //Jadkom
    Route::get('/pelatih/jadwal/jadkom/datajadkom', [\App\Http\Controllers\JadkomController::class, 'indexjadkom'])->name('indexjadkom-pelatih');
    Route::get('/pelatih/download/{file}', [\App\Http\Controllers\JadkomController::class, 'download'])->name('downloadfile-pelatih');

    //Laporan Sarpras
    Route::get('/pelatih/laporansarpras', [\App\Http\Controllers\LaporanSarprasController::class, 'indexlaporansarpras'])->name('indexlaporansarpras-pelatih');
    Route::get('/pelatih/laporansarpras/buatlaporan', [\App\Http\Controllers\LaporanSarprasController::class, 'formlaporansarpras'])->name('formlaporansarpras-pelatih');
    Route::post('/pelatih/laporansarpras/add', [\App\Http\Controllers\LaporanSarprasController::class, 'addlaporan'])->name('addlaporan-pelatih');

    //Absensi
    Route::get('/pelatih/dataabsensi{id}', [\App\Http\Controllers\AbsensiController::class, 'indexabsensiall'])->name('indexabsensiall-pelatih');
    Route::get('/pelatih/dataabsensi/pilih_jadlat', [\App\Http\Controllers\AbsensiController::class, 'pilih'])->name('pilih-pelatih');
    Route::get('/pelatih/absensi', [\App\Http\Controllers\AbsensiController::class, 'index'])->name('indexaddabsensi-pelatih');
    Route::get('/pelatih/absensi/{id}', [\App\Http\Controllers\AbsensiController::class, 'indexaddabsensi'])->name('indexaddabsensi-pelatih');
    Route::post('/pelatih/absensi/add', [\App\Http\Controllers\AbsensiController::class, 'addabsensi'])->name('addabsensi-pelatih');

    //Generate Absensi
    Route::get('/pelatih/generateabsensi', [\App\Http\Controllers\AbsensiController::class, 'indexgenerate'])->name('indexgenerate-pelatih');
    Route::get('/pelatih/generateabsensi/{id}', [\App\Http\Controllers\AbsensiController::class, 'actiongenerate'])->name('indexgenerate-pelatih');

    //Kriteria
    Route::get('/pelatih/datakriteria', [\App\Http\Controllers\KriteriaController::class, 'indexkriteria'])->name('indexkriteria-pelatih');
    Route::get('/pelatih/pilih-kategori-kriteria', [\App\Http\Controllers\KriteriaController::class, 'addkriteria'])->name('addkriteria-pelatih');
    Route::post('/pelatih/kriteria/add', [\App\Http\Controllers\KriteriaController::class, 'actionadd'])->name('actionadd-pelatih');
    Route::get('/pelatih/kriteria/delete{id}', [\App\Http\Controllers\KriteriaController::class, 'deletekriteria'])->name('deletekriteria-pelatih');
    Route::post('/pelatih/kriteria/import-excel', [\App\Http\Controllers\KriteriaController::class, 'importkriteria'])->name('importkriteria-sarpras');

    //Sub-Kriteria
    Route::get('/pelatih/data-sub_kriteria', [\App\Http\Controllers\SubkriteriaController::class, 'indexsubkriteria'])->name('indexsubkriteria-pelatih');
    Route::get('/pelatih/tambah-sub_kriteria', [\App\Http\Controllers\SubkriteriaController::class, 'addsubkriteria'])->name('addsubkriteria-pelatih');
    Route::post('/pelatih/sub_kriteria/add', [\App\Http\Controllers\SubkriteriaController::class, 'actionadd'])->name('actionadd-pelatih');
    Route::get('/pelatih/sub_kriteria/delete{id}', [\App\Http\Controllers\SubkriteriaController::class, 'deletesubkriteria'])->name('deletesubkriteria-pelatih');

    //Penilaian Atlet

    Route::get('/pelatih/form-atlet/{id_jadkom}', [\App\Http\Controllers\PenilaianController::class, 'indexformatlet'])->name('indexformatlet-pelatih');
    Route::get('/pelatih/form-atlet/{id_jadkom}/{id_atlet}', [\App\Http\Controllers\PenilaianController::class, 'indexformnilai'])->name('indexformnilai-pelatih');
    Route::post('/pelatih/form-atlet/{id_jadkom}/{id_atlet}', [\App\Http\Controllers\PenilaianController::class, 'actionadd'])->name('indexformnilai-pelatih');
    // Route::post('/pelatih/data-penilaian/add', [\App\Http\Controllers\PenilaianController::class, 'actionadd'])->name('actionadd-pelatih');
    Route::get('/pelatih/tambah-data-penilaian', [\App\Http\Controllers\PenilaianController::class, 'indexadd'])->name('indexadd-pelatih');

    //Menampilkan Data Penilaian
    Route::get('/pelatih/datapenilaian/pilih-jadkom', [\App\Http\Controllers\PenilaianController::class, 'index1'])->name('index1-pelatih');
    Route::get('/pelatih/pilih-atlet/{id_jadkom}', [\App\Http\Controllers\PenilaianController::class, 'index2'])->name('index2-pelatih');
    Route::get('/pelatih/data-penilaian', [\App\Http\Controllers\PenilaianController::class, 'indexpenilaian'])->name('indexpenilaian-pelatih');

    //Hasil
    Route::get('/pelatih/pilih-jadkom', [\App\Http\Controllers\PenilaianController::class, 'indexhasil'])->name('indexhasil-pelatih');
    Route::get('/pelatih/hasil-seleksi/{id_jadkom}', [\App\Http\Controllers\PenilaianController::class, 'hasilseleksi'])->name('hasilseleksi-pelatih');
    Route::get('/pelatih/hasil-seleksi', [\App\Http\Controllers\PenilaianController::class, 'indexpenilaian'])->name('indexpenilaian-pelatih');
});

// Atlet
Route::group(['middleware' => 'role:atlet'], function () {

    //Dashboard
    Route::get('/atlet/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardatlet'])->name('dashboard-atlet');

    //Jadlat
    Route::get('/atlet/jadwal/jadlat/datajadlat', [\App\Http\Controllers\JadlatController::class, 'indexjadlat'])->name('indexjadlat-atlet');

    //Jadkom
    Route::get('/atlet/jadwal/jadkom/datajadkom', [\App\Http\Controllers\JadkomController::class, 'indexjadkom'])->name('indexjadkom-atlet');
    Route::get('/atlet/download/{file}', [\App\Http\Controllers\JadkomController::class, 'download'])->name('downloadfile-atlet');

    //Laporan Sarpras
    Route::get('/atlet/laporansarpras', [\App\Http\Controllers\LaporanSarprasController::class, 'indexlaporansarpras'])->name('indexlaporansarpras-atlet');
    Route::get('/atlet/laporansarpras/buatlaporan', [\App\Http\Controllers\LaporanSarprasController::class, 'formlaporansarpras'])->name('formlaporansarpras-atlet');
    Route::post('/atlet/laporansarpras/add', [\App\Http\Controllers\LaporanSarprasController::class, 'addlaporan'])->name('addlaporan-atlet');
    Route::get('/atlet/downloadsop/buatlaporan', [\App\Http\Controllers\LaporanSarprasController::class, 'formlaporansarpras'])->name('formlaporansarpras-atlet');

    //Absensi
    Route::get('/atlet/dataabsensi', [\App\Http\Controllers\AbsensiController::class, 'indexabsensi'])->name('indexabsensi-atlet');

    //Gaji
    Route::get('/atlet/gaji', [\App\Http\Controllers\GajiController::class, 'indexgaji'])->name('indexgaji-atlet');

    //Penilaian
    Route::get('/atlet/pilih-jadkom/peniliaian', [\App\Http\Controllers\PenilaianController::class, 'indexhasilatlet'])->name('indexhasilatlet-atlet');
    Route::get('/atlet/data-penilaian/{id_jadkom}', [\App\Http\Controllers\PenilaianController::class, 'indexpenilaianatlet'])->name('indexpenilaianatlet-atlet');

    //Hasil
    Route::get('/atlet/pilih-jadkom', [\App\Http\Controllers\PenilaianController::class, 'indexpilihjadkom'])->name('indexpilihjadkom-atlet');
    Route::get('/atlet/hasil-seleksi/{id_jadkom}', [\App\Http\Controllers\PenilaianController::class, 'indexranking'])->name('indexranking-atlet');
});

// Sarpras
Route::group(['middleware' => 'role:sarpras'], function () {

    //Dashboard
    Route::get('/sarpras/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardsarpras'])->name('dashboard-sarpras');

    //Data Sarpras
    Route::get('/sarpras/data-sarpras', [\App\Http\Controllers\SarprasController::class, 'indexdatasarpras'])->name('indexdatasarpras-sarpras');
    Route::get('/sarpras/data-sarpras/add-sarpras', [\App\Http\Controllers\SarprasController::class, 'indexaddsarpras'])->name('indexaddsarpras-sarpras');
    Route::post('/sarpras/data-sarpras/add-sarpras/add', [App\Http\Controllers\SarprasController::class, 'addsarpras'])->name('addsarpras-sarpras');
    Route::get('/sarpras/data-sarpras/delete{id}', [\App\Http\Controllers\SarprasController::class, 'deletesarpras'])->name('deletesarpras-sarpras');
    Route::post('/sarpras/data-sarpras/import-excel', [\App\Http\Controllers\SarprasController::class, 'import'])->name('import-sarpras');
    Route::post('/sarpras/data-sarpras/export-excel', [\App\Http\Controllers\SarprasController::class, 'import'])->name('import-sarpras');

    //Laporan Sarpras
    Route::get('/sarpras/laporansarpras', [\App\Http\Controllers\LaporanSarprasController::class, 'indexdatalaporansarpras'])->name('indexdatalaporansarpras-sarpras');

    //Pengajuan
    Route::get('/sarpras/pengajuan', [\App\Http\Controllers\PengajuanSarprasController::class, 'indexpengajuan'])->name('indexpengajuan-sarpras');
    Route::post('/sarpras/pengajuan/create', [\App\Http\Controllers\PengajuanSarprasController::class, 'indexcreatepengajuan'])->name('indexcreatepengajuan-sarpras');
    Route::post('/sarpras/pengajuan/store', [App\Http\Controllers\PengajuanSarprasController::class, 'addpengajuan'])->name('addpengajuan-sarpras');
});

// Keuangan
Route::group(['middleware' => 'role:keuangan'], function () {

    //Dashboard
    Route::get('/keuangan/dashboard', [App\Http\Controllers\HomeController::class, 'dashboardkeuangan'])->name('dashboard-keuangan');

    //Pengajuan
    Route::get('/keuangan/pengajuan-sarpras', [App\Http\Controllers\PengajuanSarprasController::class, 'indexapprove'])->name('indexapprove-keuangan');
    Route::get('/keuangan/pengajuan-sarpras/acc/{id}', [App\Http\Controllers\PengajuanSarprasController::class, 'accpengajuankeuangan'])->name('accpengajuanpengajuan-keuangan');
    Route::get('/keuangan/pengajuan-sarpras/reject/{id}', [App\Http\Controllers\PengajuanSarprasController::class, 'rejectpengajuankeuangan'])->name('rejectpengajuanpengajuan-keuangan');

    //Setting Gaji
    Route::get('/keuangan/setting_gaji', [App\Http\Controllers\SettingGajiController::class, 'indexsettinggaji'])->name('indexsettinggaji-keuangan');
    Route::get('/keuangan/setting_gaji/add-kategori', [App\Http\Controllers\SettingGajiController::class, 'addkategori'])->name('addkategori-keuangan');
    Route::post('/keuangan/setting_gaji/tambah', [App\Http\Controllers\SettingGajiController::class, 'actionadd'])->name('actionadd-keuangan');
    Route::post('/keuangan/editsetgaji/{id}', [App\Http\Controllers\SettingGajiController::class, 'actionedit'])->name('indexeditsetgaji-keuangan');

    //Generate Gaji
    Route::get('/keuangan/generategaji', [\App\Http\Controllers\SettingGajiController::class, 'indexgenerate'])->name('indexgenerate-keuangan');
    Route::get('/keuangan/generategaji/add_generategaji/{id}', [\App\Http\Controllers\SettingGajiController::class, 'indexaddgenerate'])->name('indexaddgenerate-keuangan');
    Route::post('/keuangan/generategaji/actionadd', [\App\Http\Controllers\SettingGajiController::class, 'actionaddgenerate'])->name('indexaddgenerate-keuangan');

    //Pencairan
    Route::get('/keuangan/pencairan', [\App\Http\Controllers\SettingGajiController::class, 'indexpencairan'])->name('indexpencairan-keuangan');
    Route::get('/keuangan/pencairan/{id}', [\App\Http\Controllers\SettingGajiController::class, 'actionpencairan'])->name('indexpencairan-keuangan');

    //Data Keuangan

    //Saldo
    Route::get('/keuangan/data-keuangan', [\App\Http\Controllers\SaldoController::class, 'indexkeuangan'])->name('indexkeuangan-keuangan');
    Route::get('/keuangan/tambah-data-keuangan', [\App\Http\Controllers\SaldoController::class, 'indexaddkeuangan'])->name('indexaddkeuangan-keuangan');
    Route::Post('/keuangan/data-keuangan/add', [\App\Http\Controllers\SaldoController::class, 'actionadd'])->name('actionadd-keuangan');
});


require __DIR__ . '/auth.php';
